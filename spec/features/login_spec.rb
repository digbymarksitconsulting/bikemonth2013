require 'spec_helper'

describe "Admin Login Page", :type => :feature do

  #before :each do
  #  load "#{Rails.root}/db/seeds/all.rb"
  #end

  it 'should have some key fields' do
    visit '/refinery/login'
    current_path.should == refinery.new_refinery_user_session_path

    find_button('Sign in').visible?
    find_link('Sign up').visible?
    find_link('I forgot my password').visible?
    find(:xpath, '//*[@id="refinery_user_remember_me" and @type="checkbox"]').visible?
    page.has_content? 'Remember me'

    find_field('Login').visible?
    find_field('Password').visible?
  end
end