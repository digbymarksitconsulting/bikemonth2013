require 'spec_helper'

feature 'Events Page' do

  describe 'Public features' do

    before :each do

      Refinery::Events::Event.create title: "First event in a month!", blurb: "<p>This is a full day event</p>",
                                     position: 0, start_at: "2013-05-01 00:00:00 EDT", end_at: "2013-05-01 23:59:59 EDT",
                                     summary: "Summary First event in a month!",
                                     location_id: Refinery::Locations::Location.where(:slug => 'hamilton').first.id,
                                     user_id: Refinery::User.first.id, admin_authorized: true, city: 'Hamilton',
                                     contact_email: Refinery::User.first.email,
                                     refinery_events_event_type_id: Refinery::Events::EventType.last.id

      Refinery::Events::Event.create title: "Multi-day event", blurb: "<p>This is a full day event</p>",
                                     position: 1, start_at: "2013-05-05 00:00:00 EDT", end_at: "2013-05-08 23:59:59 EDT",
                                     summary: "Summary Multi-day event",
                                     location_id: Refinery::Locations::Location.where(:slug => 'etobicoke-york').first.id,
                                     user_id: Refinery::User.first.id, admin_authorized: true, city: 'Toronto',
                                     contact_email: Refinery::User.first.email,
                                     refinery_events_event_type_id: Refinery::Events::EventType.first.id

      Refinery::Events::Event.create title: "Last event in a month!", blurb: "<p>This is a full day event</p>",
                                     position: 3, start_at: "2013-05-31 00:00:00 EDT", end_at: "2013-05-31 23:59:59 EDT",
                                     summary: "Summary Last event in a month!",
                                     location_id: Refinery::Locations::Location.where(:slug => 'halton').first.id,
                                     user_id: Refinery::User.first.id, admin_authorized: true, city: 'Halton',
                                     contact_email: Refinery::User.first.email,
                                     refinery_events_event_type_id: Refinery::Events::EventType.first.id

      Refinery::Events::Event.create title: "Unauthorised!", blurb: "<p>This is an event</p>",
                                     position: 4, start_at: "2013-05-04 18:00:00", end_at: "2013-05-04 20:00:00",
                                     summary: "Summary Unauthorised!",
                                     location_id: Refinery::Locations::Location.where(:slug => 'mississauga').first.id,
                                     user_id: Refinery::User.first.id, admin_authorized: false, city: 'Mississauge',
                                     contact_email: Refinery::User.first.email,
                                     refinery_events_event_type_id: Refinery::Events::EventType.last.id

      Refinery::Events::Event.create title: "Somewhere in a month!", blurb: "<p>This is an event</p>",
                                     position: 5, start_at: "2013-05-03 18:00:00", end_at: "2013-05-03 20:00:00",
                                     summary: "Summary Somewhere in a month!",
                                     location_id: Refinery::Locations::Location.where(:slug => 'scarborough').first.id,
                                     user_id: Refinery::User.first.id, admin_authorized: true, city: 'Toronto',
                                     contact_email: Refinery::User.first.email,
                                     refinery_events_event_type_id: Refinery::Events::EventType.first.id

      Refinery::Events::Event.create title: "Duplicate Event!", blurb: "<p>This is an event</p>",
                                     position: 5, start_at: "2013-05-03 18:00:00 EDT", end_at: "2013-05-03 20:00:00 EDT",
                                     summary: "Summary Duplicate Event!",
                                     location_id: Refinery::Locations::Location.where(:slug => 'scarborough').first.id,
                                     user_id: Refinery::User.first.id, admin_authorized: true, city: 'Toronto',
                                     contact_email: Refinery::User.first.email,
                                     refinery_events_event_type_id: Refinery::Events::EventType.last.id

    end

    context "html" do
      it 'all authorised events should be displayed if the events index page is pulled' do
        visit '/events'
        expect(current_path).to eql(refinery.events_events_path)

        should have_text("First event in a month!")
        should have_text("Somewhere in a month!")
        should have_text("Duplicate Event!")
        should have_text("Multi-day event")
        should have_text("Last event in a month!")

        should_not have_text "Unauthorised!"

      end

      it 'all authorised events should be displayed if the events index page is pulled with a month and year' do
        visit '/events?month=5&year=2013'
        expect(current_path).to eql(refinery.events_events_path)

        should have_text("First event in a month!")
        should have_text("Somewhere in a month!")
        should have_text("Duplicate Event!")
        should have_text("Multi-day event")
        should have_text("Last event in a month!")

        should_not have_text "Unauthorised!"


      end

      it 'should return nothing if the month and year has no events' do
        visit '/events?month=4&year=2013'
        expect(current_path).to eql(refinery.events_events_path)

        all("*[@id='events']/li/a").count.should equal(0)

      end

      it 'all authorised events should be displayed if the events index page is pulled with a day, a month and year' do
        visit '/events?day=3&month=5&year=2013'
        expect(current_path).to eql(refinery.events_events_path)

        all("*[@id='events']/li/a").count.should equal(2)

        find("*[@id='events']/li[1]/a[text()]").should have_text("Somewhere in a month!")
        find("*[@id='events']/li[2]/a[text()]").should have_text("Duplicate Event!")

      end

      it 'all authorised events should be displayed if the events index page is pulled with a location, a day, a month and year' do
        visit '/events?day=3&month=5&year=2013&location=toronto'
        expect(current_path).to eql(refinery.events_events_path)

        all("*[@id='events']/li/a").count.should equal(2)

        find("*[@id='events']/li[1]/a[text()]").should have_text("Somewhere in a month!")
        find("*[@id='events']/li[2]/a[text()]").should have_text("Duplicate Event!")

      end
    end

    #context "json" do
    #  it 'should return json' do
    #
    #    puts Refinery::Events::Event.all.inspect
    #
    #    visit '/events.json?month=5&year=2013'
    #    expect(current_path).to eql("#{refinery.events_events_path}.json")
    #
    #    json_hash = ActiveSupport::JSON.decode(page.body)
    #
    #    json_hash["local_events"].each do |item|
    #      puts item
    #    end
    #
    #    json_hash["local_events"].length.should == 5
    #
    #  end
    #
    #end

  end

  describe 'Admin features' do

    it 'should make users have to sign in if not signed in' do
      visit '/refinery/events/'
      expect(current_path).to eq(refinery.new_refinery_user_session_path)
    end

    context 'sign in public user' do
      before :each do
        user_events =  Refinery::User.create username: "new_user", email: "aaron.romeo+new_user@gmail.com",
                                             password: "test", password_confirmation: "test",
                                             full_name: "Test Event User", phone: "647-123-1234"
        user_events.add_role :refinery
        user_events.plugins = Refinery::User.general_users_plugins
        user_events.confirm!

        visit refinery.new_refinery_user_session_path

        fill_in 'Login', :with => 'new_user'
        fill_in 'Password', :with => 'test'

        click_button 'Sign in'
      end

      it 'should take the users to the events page' do
        visit '/refinery/events'
        current_path.should == refinery.events_admin_events_path
      end

      it 'should be able able to load the new events form' do
        visit '/refinery/events'

        click_link('Add New Event')
        current_path.should == refinery.new_events_admin_event_path

        find_field('Title').visible?

      end

      context 'Test event creation' do

        before :each do
          visit '/refinery/events'

          click_link('Add New Event')
        end

        it 'should not have Admin authorized' do
          should_not have_field('Admin authorized')
        end

        it 'should have a Title' do
          find_field('Title').visible?
        end

        it 'should have a Event Type' do
          find_field('Event type').visible?
          expect(find_field('Event type').value).to eq('')
          find_field('Event type').should have_text('Choose an Event TypeArt / FilmClinic / WorkshopFestival / Special EventParty / SocialPresentation / LectureRaceRide')
        end

        it 'should have a Start at' do
          find_field('Start at').visible?
        end

        it 'should have a End at' do
          find_field('End at').visible?
        end

        it 'should have a Location' do
          find_field('Location').visible?
          expect(find_field('Location').value).to eq('')
          find_field('Location').should have_text('Choose a LocationHamiltonHaltonDurhamYorkPeel - Brampton/CaledonPeel - MississaugaToronto - Etobicoke-YorkToronto - ScarboroughToronto - North YorkToronto - Downtown-East York')
        end

        it 'should have a Summary' do
          find_field('Summary').visible?
        end

        it 'should have a Photo' do
          find_link('There is currently no image selected, please click here to add one.').visible?
          page.should have_text('Photo')
        end

        it 'should have an Address' do
          find_field('Address').visible?
        end

        it 'should have a City' do
          find_field('City').visible?
        end

        it 'should have a Nearest Major Intersection' do
          find_field('Nearest Major Intersection').visible?
        end

        it 'should have a Map' do
          page.should have_selector("#google_map_image")
          find(:xpath, '//*[@id="google_map_image"]').visible?
        end

        it 'should have an RSVP' do
          find_field('Is an RSVP Required?').visible?
          expect(find_field('Is an RSVP Required?').value).to eq('false')
          find_field('Is an RSVP Required?').should have_text('YesNo')
        end

        it 'should have an RSVP Email' do
          page.should have_xpath('event_rsvp_email')
          expect(find_field('RSVP Email')['disabled']).to eq('disabled')
        end

        it 'should have a Contact email' do
          find_field('Contact email').visible?
        end

        it 'should have a Contact phone' do
          find_field('Contact phone').visible?
        end

        it 'should have a Cost' do
          find_field('Cost').visible?
        end

        it 'should have a Event features' do
          page.should have_field('Accessible')
          page.should have_field('Partially accessible')
          page.should have_field('Parking')
          page.should have_field('Public washrooms')
          page.should have_field('Bike Parking')
          page.should have_field('On-site food or beverages')
          page.should have_field('New this year')
        end

        it 'should have a Url 1' do
          find_field('Url 1').visible?
        end

        it 'should have a Url 2' do
          find_field('Url 2').visible?
        end

        it 'should have a Url 3' do
          find_field('Url 3').visible?
        end

        it 'should have a Blurb' do
          page.should have_selector('div#blurb')
        end

        it 'should allow event creation', :js => true do
          fill_in 'event_title', :with => 'Sample Test Event'
          select 'Ride', :from => 'Event type'
          fill_in 'Start at', :with => '2013-05-04 14:00:00'
          fill_in 'End at', :with => '2013-05-04 17:00:00'
          select 'Toronto - Scarborough', :from => 'Location'

          click 'Save'
        end


      end

    end

  end

end