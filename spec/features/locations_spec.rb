require 'spec_helper'

feature "Locations" do
  describe 'Public features' do


  end

  describe 'Admin features' do

    context 'verify location setup' do
      before :each do
        Refinery::Locations::Location.count.should be > 0

        visit refinery.new_refinery_user_session_path

        fill_in 'Login', :with => 'aaron'
        fill_in 'Password', :with => 's6ay0ut!'

        click_button 'Sign in'
      end

      it 'should take the users to the locations page' do
        visit '/refinery/locations'
        current_path.should == refinery.locations_admin_locations_path
      end

      context 'Test location creation' do

        before :each do
          visit '/refinery/locations'

          click_link('Add New Location')
        end

        it 'should have a Name' do
          page.should have_field('Name')
        end

        it 'should have a Parent location' do
          find_field('Parent location').visible?
          expect(find_field('Parent location').value).to eq('')
          find_field('Parent location').should have_text('Choose a LocationAll LocationsTorontoHamiltonHaltonDurhamYorkPeelMississaugaEtobicoke-YorkScarboroughNorth YorkToronto-East York')
        end

        it 'should have a Location type' do
          find_field('Location type').visible?
          expect(find_field('Location type').value).to eq('')
          find_field('Location type').should have_text('MunicipalityRegion')
        end

        it 'should have the option to set the cyclist profile' do
          find_field('Does the location require a cyclist profile?').visible?
          expect(find_field('Does the location require a cyclist profile?').value).to eql('')
        end

        it 'should have the option to set the event calendar' do
          find_field('Does the location require an event calendar?').visible?
          expect(find_field('Does the location require an event calendar?').value).to eql('')
        end

        it 'should have a Tips And Resources Field' do
          page.should have_selector('div#tips_and_resources')
        end

        context 'should be able to create an event' do


        end

      end
      context 'Test location editing' do

        before :each do
          testloco1 = Refinery::Locations::Location.create :name=> 'Test Loco 1',
                                                         :parent_location_id =>
                                                             Refinery::Locations::Location.where(:slug => 'toronto').first.id,
                                                         :position => Refinery::Locations::Location.count+1,
                                                         :location_type => 'Municipality',
                                                         :has_cyclist_profile => true, :has_event_calendar => false

          testloco1.should be_valid

          (Refinery::Locations::Admin::LocationsController.
              set_location_page testloco1).save

          testloco2 = Refinery::Locations::Location.create :name=> 'Test Loco 2',
                                                           :parent_location_id =>
                                                               Refinery::Locations::Location.where(:slug => 'toronto').first.id,
                                                           :position => Refinery::Locations::Location.count+1,
                                                           :location_type => 'Municipality',
                                                           :has_cyclist_profile => true, :has_event_calendar => false
          testloco2.should be_valid

          (Refinery::Locations::Admin::LocationsController.
              set_location_page testloco2).save

          visit '/refinery/locations'

          page.should have_text('Test Loco 1')
          visit refinery.edit_locations_admin_location_path('test-loco-1')
        end

        it 'should have a Name' do
          page.should have_field('Name')
          expect( find_field('Name').value ).to eql('Test Loco 1')
        end

        it 'should have a Parent location' do
          find_field('Parent location').visible?
          expect(find_field('Parent location').find('option[selected]').text).to eql('Toronto')
          find_field('Parent location').should have_text('All LocationsTorontoHamiltonHaltonDurhamYorkPeel - Brampton/CaledonPeel - MississaugaToronto - Etobicoke-YorkToronto - ScarboroughToronto - North YorkToronto - Downtown-East YorkTest Loco 2')
        end

        it 'should have a Location type' do
          find_field('Location type').visible?
          expect(find_field('Location type').find('option[selected]').text).to eql('Municipality')
          find_field('Location type').should have_text('MunicipalityRegion')
        end

        it 'should have the option to set the cyclist profile' do
          find_field('Does the location require a cyclist profile?').visible?
          expect(find_field('Does the location require a cyclist profile?').value).to eql('true')
        end

        it 'should have the option to set the event calendar' do
          find_field('Does the location require an event calendar?').visible?
          expect(find_field('Does the location require an event calendar?').value).to eql('false')
        end

        it 'should have a Tips And Resources Field' do
          page.should have_selector('div#tips_and_resources')
        end

        it 'should have a public page' do
          visit '/test-loco-1'
          expect(status_code).to eql(200)
          expect(current_path).to eql('/locations/test-loco-1')
        end

        it 'should have a name independent of slug' do
          visit '/test-loco-1'
          expect(current_path).to eql('/locations/test-loco-1')

          visit refinery.edit_locations_admin_location_path('test-loco-1')
          fill_in 'Name', :with => 'foo'
          click_button 'Save'
          expect(current_path).to eql(refinery.locations_admin_locations_path)

          page.should have_text('foo')

          visit refinery.edit_locations_admin_location_path('test-loco-1')
          expect( find_field('Name').value ).to eql('foo')

          visit '/test-loco-1'
          expect(current_path).to eql('/locations/test-loco-1')

        end

      end

    end

  end

end
