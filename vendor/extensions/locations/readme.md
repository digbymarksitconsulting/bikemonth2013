# Locations extension for Refinery CMS.

## How to build this extension as a gem

    cd vendor/extensions/locations
    gem build refinerycms-locations.gemspec
    gem install refinerycms-locations.gem

    # Sign up for a http://rubygems.org/ account and publish the gem
    gem push refinerycms-locations.gem


bundle install
rails generate refinery:locations
rake db:migrate
rake db:seed
