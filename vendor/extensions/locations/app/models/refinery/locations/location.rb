module Refinery
  module Locations
    class Location < Refinery::Core::BaseModel
      extend FriendlyId
      friendly_id :initial_name, :use => [:slugged]

      before_validation :set_initial_name

      scope :with_calendar, :conditions => {:has_event_calendar => :true}

      self.table_name = 'refinery_locations'
      has_many :locations, foreign_key: :parent_location_id,
                              class_name: "Refinery::Locations::Location"

      belongs_to :parent_location,
               class_name: "Refinery::Locations::Location"

      has_many :cyclists, dependent: :destroy

      has_many :sponsors, through: :sponsor_location, :class_name => "Refinery::Sponsors::Sponsor"
      belongs_to :page,
              class_name: "Refinery::Page",
              dependent: :delete

      attr_accessible :id, :name, :parent_location_id, :position, :page_id,
                      :location_type, :has_cyclist_profile, :has_event_calendar,
                      :tips_and_resources, :cyclist_map_blurb, :event_calendar_blurb

      attr_protected :initial_name

      acts_as_indexed :fields => [:name]

      validates :name, :presence => true, :uniqueness => true
      validates :location_type, :presence => :true, :inclusion => {:in => %w(Municipality Region Top),
                                                         :message => "%{value} is not a valid type"}
      validates :parent_location_id, :presence => true, :if => Proc.new {|a| a.location_type != 'Top'}

      validates :has_cyclist_profile, :inclusion => {:in => [true, false]}
      validates :has_event_calendar, :inclusion => {:in => [true, false]}

      validates :initial_name, :presence => true, :uniqueness => true

      validates_with LocationValidator

      protected
      def set_initial_name
        self.initial_name = self.name if self.initial_name.nil? and self.new_record?
      end

    end
  end
end
