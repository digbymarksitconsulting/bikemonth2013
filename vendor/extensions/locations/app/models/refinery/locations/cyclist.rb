module Refinery
  module Locations
    class Cyclist < Refinery::Core::BaseModel

      attr_accessible :name, :occupation, :url_for_place_of_work, :age, :photo_id, :blurb, :position, :location_id

      acts_as_indexed :fields => [:name, :occupation, :url_for_place_of_work, :blurb]

      validates :name, presence: true
      #validates :occupation, presence: true
      #validates :age, presence: true, :numericality => {:greater_than => 0}
      #validates :url_for_place_of_work, url: true, allow_blank: true
      validates :photo, presence: true
      validates :location, presence: true

      belongs_to :photo, class_name: '::Refinery::Image'
      belongs_to :location

      validates_with CyclistValidator

    end
  end
end
