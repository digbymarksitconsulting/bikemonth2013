module Refinery
  module Locations
    class CyclistsController < ::ApplicationController
      include LocationsHelper

      before_filter :find_all_cyclists
      before_filter :find_page

      #def index
      #  # you can use meta fields from your model instead (e.g. browser_title)
      #  # by swapping @page for @cyclist in the line below:
      #  present(@page)
      #end

      def show
        @location, @other_locations = get_current_and_other_locations(params[:location])

        @cyclist = Cyclist.find(params[:id])

        # you can use meta fields from your model instead (e.g. browser_title)
        # by swapping @page for @cyclist in the line below:
        present(@page)
      end

      protected

      def find_all_cyclists
        @cyclists = Cyclist.order('position ASC')
      end

      def find_page
        @page = ::Refinery::Page.where(:link_url => "/cyclists").first
      end

    end
  end
end
