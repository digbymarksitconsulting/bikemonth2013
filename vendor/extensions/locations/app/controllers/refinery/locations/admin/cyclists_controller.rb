module Refinery
  module Locations
    module Admin
      class CyclistsController < ::Refinery::AdminController
        helper :all

        crudify :'refinery/locations/cyclist',
                :title_attribute => 'name', :xhr_paging => true

        #def create
        #  # if the position field exists, set this object as last object, given the conditions of this class.
        #  if Refinery::Locations::Cyclist.column_names.include?("position") && params[:cyclist][:position].nil?
        #    params[:location].merge!({
        #                                 :position => ((Refinery::Locations::Location.maximum(:position, :conditions => "")||-1) + 1)
        #                             })
        #  end
        #
        #  @location = Refinery::Locations::Admin::LocationsController.
        #      set_location_page Refinery::Locations::Location.new(params[:location])
        #  @location.save unless @location.nil?
        #
        #  if @location.valid?
        #    Refinery::Pages.default_parts.each_with_index do |default_page_part, index|
        #      @location.page.parts.create(:title => default_page_part, :body => nil, :position => index)
        #    end
        #
        #    flash.notice = t(
        #        'refinery.crudify.created',
        #        :what => "'#{@location.name}'"
        #    )
        #
        #    redirect_back_or_default(refinery.locations_admin_locations_path)
        #  else
        #    unless request.xhr?
        #      render :action => 'new'
        #    else
        #      render_create_error @location
        #    end
        #  end
        #end

      end
    end
  end
end
