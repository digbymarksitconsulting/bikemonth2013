module Refinery
  module Locations
    module Admin
      class LocationsController < ::Refinery::AdminController
        helper :all

        crudify :'refinery/locations/location',
                :title_attribute => 'name', :xhr_paging => true

        def create
          # if the position field exists, set this object as last object, given the conditions of this class.
          if Refinery::Locations::Location.column_names.include?("position") && params[:location][:position].nil?
            params[:location].merge!({
                                         :position => ((Refinery::Locations::Location.maximum(:position, :conditions => "")||-1) + 1)
                                     })
          end

          @location = Refinery::Locations::Admin::LocationsController.
              set_location_page Refinery::Locations::Location.new params[:location]
          #@location = Refinery::Locations::Location.new params[:location]
          @location.save unless @location.nil?

          if @location.valid?
            flash.notice = t(
                'refinery.crudify.created',
                :what => "'#{@location.name}'"
            )

            redirect_back_or_default(refinery.locations_admin_locations_path)
          else
            unless request.xhr?
              render :action => 'new'
            else
              render_create_error @location
            end
          end
        end

        def destroy
          # object gets found by find_location function
          title = @location.name
          if @location.destroy
            flash.notice = t('destroyed', :scope => 'refinery.crudify', :what => "'#{title}'")
          end

          redirect_to refinery.locations_admin_locations_path
        end

        def self.set_location_page(location)
          url = "/#{(location.initial_name).to_slug.normalize}"
          if (!location.save || Refinery::Page.where(:slug => url).present? || !location.valid?)
            location.destroy if location.valid?
            return location
          end

          location.page = Refinery::Page.create(
              :title => "#{(location.name).camelcase}",
              :slug => "#{(location.initial_name).downcase}",
              :link_url => Refinery::Core::Engine.routes.url_helpers.locations_location_path(location),
              :deletable => false,
              :show_in_menu => false
              #:menu_match => "^#{url}(\/|\/.+?|)$"
          )
          Refinery::Pages.default_parts.each_with_index do |default_page_part, index|
            location.page.parts.create(:title => default_page_part, :body => nil, :position => index)
          end

          return location
        end

        protected
        def render_create_error(object)
          render :partial => '/refinery/admin/error_messages', :locals => {
              :object => object,
              :include_object_name => true
          }
        end
      end
    end
  end
end
