module Refinery
  module Locations
    class LocationsController < ::ApplicationController
      include GenericExternalHelperHelper

      before_filter :find_all_locations
      before_filter :find_page

      def index
        # you can use meta fields from your model instead (e.g. browser_title)
        # by swapping @page for @location in the line below:
        respond_to do |format|
          format.html { redirect_to '/' }
          format.json do
            render :json => Refinery::Locations::Location.all(:select => [:slug])
          end
        end
      end

      def show
        @location = Location.find(params[:id], :include => :cyclists) if @location.nil?
        @childLocations = Location.where("parent_location_id = ?", @location.id)
        @otherLocations = Location.where("parent_location_id = 1 AND id != ?", @location.id)
        @parentLocation = Location.find(@location.parent_location_id)
        @profiles = Refinery::Locations::Cyclist.all

        # get partners
        @location_param = params[:id]
        @partners = get_partners @location_param

        # you can use meta fields from your model instead (e.g. browser_title)
        # by swapping @page for @location in the line below:
        respond_to do |format|
          format.html { present(@page) }
          format.json do
            cyclists = get_cyclists Refinery::Locations::Location.find(1)
            #json_cyclist =
            json_returned = {
                location: @location.name,
                cyclists: cyclists
            }
            render :json => json_returned
          end
        end
      end

    protected

      def find_all_locations
        @locations = Location.order('position ASC')
      end

      def find_page
        @page = ::Refinery::Page.where(:link_url => "/locations").first
      end

      def get_cyclists(location)
        if location.nil?
          return
        end
        children = Refinery::Locations::Location.where(:parent_location_id => location.id).all
        if children.empty?
          cyclists = location.cyclists.all(:include => :photo, :select => [:id, :name, :photo_id])
          update_cyclists_photo_and_url(cyclists)
          return cyclists
        else
          cyclists = location.cyclists.all(:include => :photo, :select => [:id, :name, :photo_id])
          update_cyclists_photo_and_url(cyclists)
          children.each do |child|
            arr = get_cyclists(child)
            cyclists << arr unless arr.empty?
          end
          return cyclists
        end
      end

      def update_cyclists_photo_and_url(cyclists)
        cyclists.each do |cyclist|
          cyclist[:photo_url] = cyclist.photo.url
          cyclist[:url] = Refinery::Core::Engine.routes.url_helpers.locations_cyclist_path(cyclist)
        end
      end

    end
  end
end
