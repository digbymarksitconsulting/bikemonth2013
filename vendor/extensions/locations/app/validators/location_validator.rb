require 'uri'

class LocationValidator < ActiveModel::Validator
  def validate(record)
    if record.location_type=='Municipality' && !record.parent_location_id.nil? &&
        (Refinery::Locations::Location.find(record.parent_location_id).location_type == 'Top' ||
           Refinery::Locations::Location.find(record.parent_location_id).location_type == 'Region')
      return true
    elsif record.location_type=='Region'&& !record.parent_location_id.nil? &&
        Refinery::Locations::Location.find(record.parent_location_id).location_type == 'Top'
      return true
    elsif record.location_type=='Top' && record.parent_location_id.nil?
      return true
    end
    record.errors[:base] << "The location of #{record.location_type} cannot have the parent location selected"
  end

  #def validate_each(record, attribute, value)
  #  record.errors[attribute] << (options[:message] || "must be a valid Location") unless location_type_valid?(value)
  #end
  #
  #def location_type_valid?(value)
  #  if location_type=='Municipality' && !parent_location_id.nil? &&
  #      (Refinery::Locations::Location.find(parent_location_id).location_type == 'Top' ||
  #          Refinery::Locations::Location.find(parent_location_id).location_type == 'Region')
  #    true
  #  elsif location_type=='Region'&& !parent_location_id.nil? &&
  #      Refinery::Locations::Location.find(parent_location_id).location_type == 'Top'
  #    true
  #  elsif location_type=='Top' && parent_location_id.nil?
  #    true
  #  end
  #  false
  #end

end