require 'uri'

class CyclistValidator < ActiveModel::Validator
  def validate(record)
    if Refinery::Locations::Location.find(record.location_id).has_cyclist_profile
      return true
    end

    record.errors[:base] << "This location does not allow cyclist profiles"
  end

  #def validate_each(record, attribute, value)
  #  record.errors[attribute] << (options[:message] || "must be a valid Location") unless location_type_valid?(value)
  #end
  #
  #def location_type_valid?(value)
  #  if location_type=='Municipality' && !parent_location_id.nil? &&
  #      (Refinery::Locations::Location.find(parent_location_id).location_type == 'Top' ||
  #          Refinery::Locations::Location.find(parent_location_id).location_type == 'Region')
  #    true
  #  elsif location_type=='Region'&& !parent_location_id.nil? &&
  #      Refinery::Locations::Location.find(parent_location_id).location_type == 'Top'
  #    true
  #  elsif location_type=='Top' && parent_location_id.nil?
  #    true
  #  end
  #  false
  #end

end