module LocationsHelper
  def get_other_locations
    if @location.present? && @location.id.present?
      @otherLocations = Refinery::Locations::Location.where("id <> ?", @location.id).order(:id).all
    else
      @otherLocations = Refinery::Locations::Location.order(:id).all
    end
  end

  def get_all_locations
    @locations = Refinery::Locations::Location.order(:id).all
  end

  def get_locations_with_cyclists
    @locations = Refinery::Locations::Location.where(:has_cyclist_profile => :true).all
  end

  def get_current_and_other_locations(param_location, default_location=false)
    if param_location.present?
      current_location = Refinery::Locations::Location.where(:slug => param_location).first
    elsif default_location
      redirect_to '/'
      return
    end

    current_location = Refinery::Locations::Location.first if current_location.nil?

    other_locations = Refinery::Locations::Location.where(
        "parent_location_id = 1 AND id != ?", current_location.id
    ).all
    return current_location, other_locations
  end

end
