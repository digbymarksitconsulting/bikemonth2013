module Refinery
  module Locations
    class Engine < Rails::Engine
      include Refinery::Engine
      isolate_namespace Refinery::Locations

      engine_name :refinery_locations

      initializer "register refinerycms_cyclists plugin" do
        Refinery::Plugin.register do |plugin|
          plugin.name = "cyclists"
          plugin.url = proc { Refinery::Core::Engine.routes.url_helpers.locations_admin_cyclists_path }
          plugin.pathname = root
          plugin.activity = {
            :class_name => :'refinery/locations/cyclist',
            :title => 'name'
          }
          plugin.menu_match = %r{refinery/locations/cyclists(/.*)?$}
        end
      end

      config.after_initialize do
        Refinery.register_extension(Refinery::Cyclists)
      end
    end
  end
end
