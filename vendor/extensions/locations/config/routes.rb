Refinery::Core::Engine.routes.append do

  # Frontend routes
  namespace :locations do
    resources :locations, :path => '', :only => [:show, :index]
  end

  # Admin routes
  namespace :locations, :path => '' do
    namespace :admin, :path => 'refinery' do
      resources :locations, :except => :show do
        collection do
          post :update_positions
        end
      end
    end
  end


  # Frontend routes
  namespace :locations do
    resources :cyclists, :only => [:show]
  end

  # Admin routes
  namespace :locations, :path => '' do
    namespace :admin, :path => 'refinery/locations' do
      resources :cyclists, :except => :show do
        collection do
          post :update_positions
        end
      end
    end
  end

end
