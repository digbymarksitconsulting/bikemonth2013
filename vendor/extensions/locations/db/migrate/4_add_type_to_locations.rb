class AddTypeToLocations < ActiveRecord::Migration
  def change
    add_column :refinery_locations, :location_type, :string
  end
end