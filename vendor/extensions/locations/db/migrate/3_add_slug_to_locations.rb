class AddSlugToLocations < ActiveRecord::Migration
  def change
    add_column :refinery_locations, :slug, :string
    add_index :refinery_locations, :slug
  end
end