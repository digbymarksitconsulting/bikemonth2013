class CreateLocationsCyclists < ActiveRecord::Migration

  def up
    create_table :refinery_locations_cyclists do |t|
      t.string :name
      t.string :occupation
      t.string :url_for_place_of_work
      t.integer :age
      t.integer :photo_id
      t.integer :location_id
      t.text :blurb
      t.integer :position

      t.timestamps
    end

  end

  def down
    if defined?(::Refinery::UserPlugin)
      ::Refinery::UserPlugin.destroy_all({:name => "refinerycms-locations"})
    end

    if defined?(::Refinery::Page)
      ::Refinery::Page.delete_all({:link_url => "/locations/cyclists"})
    end

    drop_table :refinery_locations_cyclists

  end

end
