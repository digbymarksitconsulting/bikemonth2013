class AddPermissionsToLocations < ActiveRecord::Migration
  def change
    add_column :refinery_locations, :has_cyclist_profile, :boolean
    add_column :refinery_locations, :has_event_calendar, :boolean
  end
end