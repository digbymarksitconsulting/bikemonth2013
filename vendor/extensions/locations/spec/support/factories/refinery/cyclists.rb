
FactoryGirl.define do
  factory :cyclist, :class => Refinery::Locations::Cyclist do
    sequence(:name) { |n| "refinery#{n}" }
  end
end

