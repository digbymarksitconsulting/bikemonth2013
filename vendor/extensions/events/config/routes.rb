Refinery::Core::Engine.routes.append do

  match '/events/events/:year/:month' => 'events/events#index',
        constraints: {:year => /\d{4}/, :month => /\d{1,2}/}, via: :get,
        controller: :events

  match ':location/feed' => 'events/events#feed',
        :as => :feed,
        :defaults => { :format => 'atom' }, via: :get,
        controller: :events

  # Frontend routes
  namespace :events do
    resources :events, :path => '', :only => [:index, :show]
  end

  # Admin routes
  namespace :events, :path => '' do
    namespace :admin, :path => 'refinery' do
      resources :events, :except => :show do
        collection do
          post :update_positions
          get :geocode
        end
      end
    end
  end


  # Admin routes
  namespace :events, :path => '' do
    namespace :admin, :path => 'refinery/events' do
      resources :event_types, :except => :show do
        collection do
          post :update_positions
        end
      end
    end
  end


  # Admin routes
  namespace :events, :path => '' do
    namespace :admin, :path => 'refinery/events' do
      resources :event_features, :except => :show do
        collection do
          post :update_positions
        end
      end
    end
  end

end
