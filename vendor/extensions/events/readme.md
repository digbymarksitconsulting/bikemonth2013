# Events extension for Refinery CMS.

## How to build this extension as a gem

    cd vendor/extensions/events
    gem build refinerycms-events.gemspec
    gem install refinerycms-events.gem

    # Sign up for a http://rubygems.org/ account and publish the gem
    gem push refinerycms-events.gem

rails g refinery:engine event_type name:string --extension events --namespace events --skip-frontend
rails g refinery:engine event_feature name:string --extension events --namespace events --skip-frontend

bundle install
rails generate refinery:events
rake db:migrate
rake db:seed
