
FactoryGirl.define do
  factory :event_feature, :class => Refinery::Events::EventFeature do
    sequence(:name) { |n| "refinery#{n}" }
  end
end

