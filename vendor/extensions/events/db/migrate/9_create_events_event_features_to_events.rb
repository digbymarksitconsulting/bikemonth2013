class CreateEventsEventFeaturesToEvents < ActiveRecord::Migration

  def up
    create_table :refinery_events_event_features_to_refinery_events_events, :id => false  do |t|
      t.references :refinery_events_event_features
      t.references :refinery_events

      t.timestamps
    end

  end

  def down
    if defined?(::Refinery::UserPlugin)
      ::Refinery::UserPlugin.destroy_all({:name => "refinerycms-events"})
    end

    drop_table :refinery_events_event_features_to_refinery_events_events

  end

end
