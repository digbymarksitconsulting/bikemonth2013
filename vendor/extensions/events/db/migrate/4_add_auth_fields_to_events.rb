class AddAuthFieldsToEvents < ActiveRecord::Migration
  def change
    add_column :refinery_events, :admin_authorized, :boolean
  end
end