class ChangeDateFieldsForEvents < ActiveRecord::Migration
  def change
    remove_column :refinery_events, :date
    add_column :refinery_events, :start_at, :datetime
    add_column :refinery_events, :end_at, :datetime
    add_index :refinery_events, :start_at
    add_index :refinery_events, :end_at
  end
end