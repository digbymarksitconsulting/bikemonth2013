module Refinery
  module Events
    class EventType < Refinery::Core::BaseModel
      self.table_name = 'refinery_events_event_types'

      has_many :refinery_events_events,
                              :class_name => 'Refinery::Events::Event',
                              :foreign_key => 'refinery_events_event_type_id'


      attr_accessible :name, :position, :display

      acts_as_indexed :fields => [:name]

      validates :name, :presence => true, :uniqueness => true
      validates :display, :inclusion => {:in => [true, false]}
    end
  end
end
