module Refinery
  module Events
    class Event < Refinery::Core::BaseModel
      scope :approved, where(:admin_authorized => true)
      after_initialize :init
      after_save :send_notification_after_change
      geocoded_by :full_address
      after_validation :geocode

      self.table_name = 'refinery_events'
      belongs_to :user, :class_name => 'Refinery::User'
      has_and_belongs_to_many :event_features, :class_name => 'Refinery::Events::EventFeature',
                              :association_foreign_key => 'refinery_events_event_features_id',
                              :join_table => 'refinery_events_event_features_to_refinery_events_events',
                              :foreign_key => 'refinery_events_id'
      belongs_to :event_type, :class_name => 'Refinery::Events::EventType', :foreign_key => 'refinery_events_event_type_id'
      belongs_to :location, :class_name => 'Refinery::Locations::Location'
      belongs_to :photo, :class_name => '::Refinery::Image'

      attr_accessible :title, :start_at, :end_at, :blurb, :position,
                      :location_id, :user_id, :admin_authorized, :photo_id,
                      :admin_authorized, :city, :contact_email, :contact_phone,
                      :cost, :refinery_events_event_type_id, :latitude, :address, :address2, :longitude,
                      :major_intersection, :rsvp_email, :title, :url_1, :url_2, :url_3,
                      :location_address_or_landmark, :summary, :event_feature_ids, :rsvp_required

      acts_as_indexed :fields => [:title, :blurb]

      alias_attribute :location_address_or_landmark, :location_raw

      validates :title, :presence => true, :uniqueness => true, :length => {:maximum => 100}
      validates :start_at, :presence => true
      validates :end_at, :presence => true
      validate :end_date_is_past_start_date

      validates :location_id, :presence => true
      validates :summary, :presence => true, :length => {:maximum => 200}

      validates :user_id, :presence => true
      validates :admin_authorized, :inclusion => {:in => [true, false]}
      validates :city, :presence => true
      validates :contact_email, :presence => { :message => "requires an email or phone for contact info" },
                :unless => Proc.new { |a| a.contact_phone.present? },
                :format => { :with  => Devise.email_regexp,
                             :message => "is not in the correct format" }
      validates :contact_phone, :presence => { :message => "requires an email or phone for contact info" },
                :unless => Proc.new { |a| a.contact_email.present? },
                :format => { :with  => BikeMonth2013Constants::PHONE_VALIDATE_REGEX,
                             :message => "is not in the format of 555-555-5555[x5555]" }
      validates :refinery_events_event_type_id, :presence => true
      validates :rsvp_required, :inclusion => {:in => [true, false]}

      validates :rsvp_email, :format => { :with  => Devise.email_regexp,
                            :message => "is not in the correct format" },
                :if => Proc.new{|a| a.rsvp_email.present? }

      validate :rsvp_check

      def init
        self.admin_authorized ||= false
      end

      def start_time
        start_at.to_i
      end

      def end_time
        end_at.to_i
      end

      def rsvp_required=(bool)
        @rsvp_is_expected = bool
        unless @rsvp_is_expected==true
          self.update_attributes(:rsvp_email => '')
        end
      end

      def rsvp_required
        self.rsvp_email.present?
      end

      def send_notification_after_change
        attribs_changed = []
        self.attribute_names.each do |attrib|
          if eval("#{attrib}_changed?") && attrib != 'updated_at'
            attribs_changed << attrib
          end
        end

        Refinery::UserMailer.admin_notification(self, self.title,  attribs_changed, self.id_changed?).deliver unless
            (self.admin_authorized_changed? && !self.id_changed?) || attribs_changed.empty?
      end

      def sanitize_for_json_creation

        location = nil
        location = Refinery::Locations::Location.where(:id => location_id).select("name").first.name if location_id.present?

        event_type = nil
        event_type = Refinery::Events::EventType.find(self.refinery_events_event_type_id) if
            self.refinery_events_event_type_id.present?

        photo_url = nil
        photo_url = photo.url if photo_id.present?

        {:title => self.title, :blurb => self.blurb, :start_time => self.start_time.to_i, :end_time => self.end_time.to_i,
          :event_type => event_type, :summary => summary, :location => location, :cost => cost, :major_intersection => major_intersection,
          :city => city, :contact_email => contact_email, :contact_phone => contact_phone, :latitude => latitude, :longitude => longitude,
          :address => address, :address2 => address2, :photo_url => photo_url, :rsvp_required => self.rsvp_required,
          :url_1 => url_1, :url_2 => url_2, :url_3 => url_3, :id => id
        }

        #self
      end

      def self.get_events_hash(locations, year, month, day, num_of_days)

        events, start_time, end_time = get_events(locations, year, month, day, num_of_days)
        events_hash = {}
        events_hash[:dates] = {:start => start_time.to_i, :end => end_time.to_i}

        ###
        # updated -- go from >= may 1 to < July 1 (end of june inclusive)
        ###
        current_time = Time.local(2013,5,1)
        end_time = Time.local(2013,7,1)

        while current_time < end_time do

          # Is there an event which is still active when my
          # day starts or starts and ends before my day ends
          day_start = Time.local(current_time.year, current_time.month, current_time.day, 0, 0, 0)
          day_end = Time.local(current_time.year, current_time.month, current_time.day, 23, 59, 59)
          events_passed = []
          events.each do |event|
            if event.start_at > day_end
              break
            end

            if (event.end_at.between? day_start, day_end) || (event.start_at < day_end && event.end_at > day_end)

              formatted_date = day_start.strftime "%A, %B %e"

              unless events_hash[formatted_date].present?
                start_time_day_month = day_start.strftime("%m_%e").delete(' ')
                events_hash[formatted_date] = {
                  :h3_selector => "DateHeader_#{ start_time_day_month }",
                  :events => [event.sanitize_for_json_creation]
                }
              else
                events_hash[formatted_date][:events] << event.sanitize_for_json_creation
              end

            end

            if event.end_at < day_start
              events_passed << event
            end
          end

          events_passed.each do |event|
            events.delete event
          end

          current_time = current_time + (60 * 60 * 24)
        end

        return events_hash

      end

      def self.get_events(locations, year, month, day, num_of_days, order = "start_at ASC")
        start_time = Time.local(year, month, day, 0, 0, 0)
        end_time = start_time + (num_of_days * 24 * 60 * 60) - 1

        events = Refinery::Events::Event.where('? <= end_at AND start_at < ? AND location_id in (?) AND admin_authorized=true',
                                               start_time, end_time, locations).order(order).all
        return events, start_time, end_time
      end

      protected
      def end_date_is_past_start_date
        if !end_at || !start_at || end_at <= start_at
          errors.add(:start_at, 'should be before the end time.')
          errors.add(:end_at, 'should be after the start time.')
        end
      end

      def rsvp_check
        if @rsvp_is_expected==true && ( !self.rsvp_email.present? || self.rsvp_email=='')
          puts "@rsvp_is_expected #{@rsvp_is_expected}"
          errors.add(:rsvp_email, 'If an RSVP is required please enter an email address')
        end
      end

      def full_address
        "#{address}, #{city}, ONTARIO, CANADA"
      end
    end
  end
end
