module Refinery
  module Events
    class EventFeature < Refinery::Core::BaseModel
      self.table_name = 'refinery_events_event_features'

      has_and_belongs_to_many :refinery_events_events,
                              :join_table => 'refinery_events_event_features_to_refinery_events_events',
                              :class_name => 'Refinery::Events::Event',
                              :association_foreign_key => 'refinery_events_id',
                              :foreign_key => 'refinery_events_event_features_id'


                              attr_accessible :name, :position, :display

      acts_as_indexed :fields => [:name]

      validates :name, :presence => true, :uniqueness => true
      validates :display, :inclusion => {:in => [true, false]}
    end
  end
end
