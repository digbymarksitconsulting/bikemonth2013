module Refinery
  module Events
    module Admin
      class EventFeaturesController < ::Refinery::AdminController

        crudify :'refinery/events/event_feature',
                :title_attribute => 'name', :xhr_paging => true

      end
    end
  end
end
