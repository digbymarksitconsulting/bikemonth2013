module Refinery
  module Events
    module Admin
      class EventsController < ::Refinery::AdminController
        helper :all
        #before_filter :current_user, :only => [:create, :update]
        before_filter :current_user, :only => [:create]

        crudify :'refinery/events/event', :xhr_paging => true

        def create
          # if the position field exists, set this object as last object, given the conditions of this class.
          #if Refinery::Events::Event.column_names.include?("position") && params[:event][:position].nil?
          #end

          if (@event = Refinery::Events::Event.create(params[:event])).valid?
            flash.notice = t(
                'refinery.crudify.created',
                :what => "#{@event.title}"
            )

            unless from_dialog?
              unless params[:continue_editing] =~ /true|on|1/
                redirect_back_or_default(refinery.events_admin_events_path)
              else
                unless request.xhr?
                  redirect_to :back
                else
                  render :partial => '/refinery/message'
                end
              end
            else
              self.index
              @dialog_successful = true
              render :index
            end
          else
            unless request.xhr?
              render :action => 'new'
            else
              render :partial => '/refinery/admin/error_messages', :locals => {
                  :object => @event,
                  :include_object_name => true
              }
            end
          end
        end

        def update
          if @event.update_attributes(params[:event])
            flash.notice = t(
                'refinery.crudify.updated',
                :what => "#{@event.title}"
            )

            unless from_dialog?
              unless params[:continue_editing] =~ /true|on|1/
                redirect_back_or_default(refinery.events_admin_events_path)
              else
                unless request.xhr?
                  redirect_to :back
                else
                  render :partial => '/refinery/message'
                end
              end
            else
              self.index
              @dialog_successful = true
              render :index
            end
          else
            unless request.xhr?
              render :action => 'edit'
            else
              render :partial => '/refinery/admin/error_messages', :locals => {
                  :object => @event,
                  :include_object_name => true
              }
            end
          end
        end

        def find_all_events(conditions = [])
          conditions << "user_id=#{current_refinery_user.id}" if !current_refinery_user.has_role?(:superuser)
          order_by = current_refinery_user.has_role?(:superuser) ?
              "admin_authorized, start_at ASC" : "start_at ASC"

          @events =
              Refinery::Events::Event.where(conditions).includes().order(order_by)
        end

        def geocode
          if params[:event][:id].present?
            @event = Refinery::Events::Event.find(params[:event][:id])
          else
            @event = Refinery::Events::Event.new
          end
          @event.address = params[:event][:address]
          @event.city = params[:event][:city]
          @event.valid?

          respond_to :js
        end

        def current_user
          params[:event].merge!({
                                    :user_id => current_refinery_user.id
                                })
        end

      end
    end
  end
end
