module Refinery
  module Events
    class EventsController < ::ApplicationController
      include GenericExternalHelperHelper
      include LocationsHelper

      #before_filter :find_all_events, :only => [:show, :index]
      before_filter :find_page

      def index
        # you can use meta fields from your model instead (e.g. browser_title)
        # by swapping @page for @event in the line below:

        present(@page)

        params[:location] ||= 'all-locations'

        # get partners
        @location_param = params[:location]
        @partners = get_partners @location_param

        @location, @other_locations = get_current_and_other_locations(@location_param)

        if params[:year].present? && params[:month].present?
          @month = params[:month].to_i
          @year = params[:year].to_i
          if params[:day].present?
            num_of_days = 1
            @day = params[:day].to_i
          else
            num_of_days = Time.days_in_month(@month, @year)
            @day = 1
          end
        else
          num_of_days = 365
          @day = 1
          @month = 1
          @year = (Time.zone || Time).now.year.to_i
        end

        locations = get_location_tree @location.id

        respond_to do |format|
          format.html do
            @events = Refinery::Events::Event.get_events_hash locations, @year, @month, @day, num_of_days
            @event_types = Refinery::Events::EventType.all
          end
        end

      end

      def show

        @event = Event.find(params[:id])
        @location, @other_locations = get_current_and_other_locations(params[:location])

        # you can use meta fields from your model instead (e.g. browser_title)
        # by swapping @page for @event in the line below:
        if @event.admin_authorized
          present(@page)
        else
          redirect_to refinery.root_path
        end
      end

      def feed
        params[:location] ||= 'all-locations'
        num_of_days = 365
        @day = 1
        @month = 1
        @year = (Time.zone || Time).now.year.to_i
        @location, @other_locations = get_current_and_other_locations(params[:location])
        locations = get_location_tree @location.id

        # this will be the name of the feed displayed on the feed reader
        @title = "FEED: Events for #{params[:location]}"

        # the news items
        @news_items, st, et = Refinery::Events::Event.get_events locations, @year, @month, @day, num_of_days, "updated_at"

        # this will be our Feed's update timestamp
        @updated = @news_items.first.updated_at unless @news_items.empty?

        respond_to do |format|
          format.atom { render :layout => false }

          # we want the RSS feed to redirect permanently to the ATOM feed
          format.rss { redirect_to feed_path(:format => :atom), :status => :moved_permanently }
        end
      end

      protected

      #def find_all_events
      #end

      def find_page
        @page = ::Refinery::Page.where(:link_url => "/events").first
      end

      def get_location_tree(location_id)
        children = Refinery::Locations::Location.where(:parent_location_id => location_id).all
        if !children.empty?
          all_locations = [location_id]
          children.each do |child|
            arr = get_location_tree child.id
            all_locations << arr
          end
          return all_locations.flatten
        end
        return location_id
      end
    end
  end
end
