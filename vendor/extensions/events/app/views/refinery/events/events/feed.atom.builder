atom_feed :language => 'en-US' do |feed|
  feed.title @title
  feed.updated @updated

  @news_items.each do |item|
    next if item.updated_at.blank?

    feed.entry( item, {:url => refinery.events_event_url(item)} ) do |entry|
      entry.url refinery.events_event_url(item)
      entry.title item.title
      entry.content "<p>Time: #{item.start_at.strftime('%A, %B %e at %l:%M %P')} - #{item.end_at.strftime('%A, %B %e at %l:%M %P')}</p>
                    <p>Location: #{item.location.name}</p>
                    <p>Summary: #{item.summary}</p>", :type => 'html'

      # the strftime is needed to work with Google Reader.
      entry.updated(item.updated_at.strftime("%Y-%m-%dT%H:%M:%SZ"))

    end
  end
end