module Refinery
  module Events
    class Engine < Rails::Engine
      include Refinery::Engine
      isolate_namespace Refinery::Events

      engine_name :refinery_events

      initializer "register refinerycms_event_features plugin" do
        Refinery::Plugin.register do |plugin|
          plugin.name = "event_features"
          plugin.url = proc { Refinery::Core::Engine.routes.url_helpers.events_admin_event_features_path }
          plugin.pathname = root
          plugin.activity = {
            :class_name => :'refinery/events/event_feature',
            :title => 'name'
          }
          plugin.menu_match = %r{refinery/events/event_features(/.*)?$}
        end
      end

      config.after_initialize do
        Refinery.register_extension(Refinery::EventFeatures)
      end
    end
  end
end
