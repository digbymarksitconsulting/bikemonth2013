# Partners extension for Refinery CMS.

## How to build this extension as a gem

    cd vendor/extensions/partners
    gem build refinerycms-partners.gemspec
    gem install refinerycms-partners.gem

    # Sign up for a http://rubygems.org/ account and publish the gem
    gem push refinerycms-partners.gem


## Creation and installation
  rails generate refinery:engine partner name:string logo:image url_path_regex:string --skip-frontend

  bundle install
  rails generate refinery:partners
  rake db:migrate
  rake db:seed