module GenericExternalHelperHelper

  def get_partners(location)
    location_param = location
    url = "/locations/#{location_param}"
    return_val = []
    partners = Refinery::Partners::Partner.all
    partners.each do |partner|
      regex = Regexp.new(CGI.unescape partner[:url_path_regex])
      if regex.match(url) && partner.logo.present?
        l = partner.logo
        if l.logo_sized_image.present?
          return_val << {
              :imagePath => l.logo_sized_image.url,
              :width => l.logo_sized_image.width,
              :height => l.logo_sized_image.height,
              :link => partner.sponsor_link
          }
        #else
        #  return_val << {
        #      :imagePath => l.thumbnail('x55').url,
        #      :width => ((l.image_width * 55) / l.image_height).ceil,
        #      :height => 55,
        #      :link => partner.sponsor_link
        #  }
        end
      end
    end
    return_val
  end

end
