class PartnersController < ApplicationController

  def show
    url = params[:url]

    logos = []
    partners = Refinery::Partners::Partner.all

    partners.each do |partner|
      regex = Regexp.new(CGI.unescape partner[:url_path_regex])
      if regex.match(url)
        l = partner.logo
        logos << {
          :imagePath => l.url,
          :width => l.image_width,
          :height => l.image_height
        }
      end
    end

    render :json => logos
  end

end
