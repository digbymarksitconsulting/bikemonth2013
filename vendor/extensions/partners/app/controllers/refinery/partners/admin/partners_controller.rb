module Refinery
  module Partners
    module Admin
      class PartnersController < ::Refinery::AdminController
        before_filter :get_all_partner_levels

        crudify :'refinery/partners/partner',
                :title_attribute => 'name', :xhr_paging => true, :order => 'name'

        def get_all_partner_levels()
          @partner_levels = {
            'All Banners' => CGI.escape('.*'),
            'Home Page Only' => CGI.escape('^/locations/$')
          }

          Refinery::Locations::Location.all.each do |location|
            @partner_levels["#{location.name} Banners"] =
                CGI.escape Refinery::Core::Engine.routes.url_helpers.locations_location_path(location).gsub("\/", "\\\/") + '$'
          end
          
          #Refinery::Locations::Location.where(:has_event_calendar => :true).all.each do |location|
          #  @partner_levels["#{location.name} Calendar"] = CGI.escape ".*location=#{location.name}.*"
          #end

          @partner_levels['Pledge Banner'] = CGI.escape ".*pledge.*"

        end

      end
    end
  end
end
