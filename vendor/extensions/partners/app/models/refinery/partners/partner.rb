module Refinery
  module Partners
    class Partner < Refinery::Core::BaseModel
      self.table_name = 'refinery_partners'

      attr_accessible :name, :logo_id, :url_path_regex, :sponsor_link,
                      :position, :about_section_id, :about_section
      
      acts_as_indexed :fields => [:name, :url_path_regex]

      validates :name, :presence => true, :uniqueness => true
      validates :logo_id, :presence => true
      validates :logo, :presence => true
      validates :url_path_regex, :presence => true
      validates :sponsor_link, :url => true, :allow_blank => true

      belongs_to :logo, :class_name => '::Refinery::Image'
      belongs_to :about_section
      
    end
  end
end
