Refinery::Core::Engine.routes.prepend do
  get '/partners', :to => 'partners#show', :as => :partners
end

Refinery::Core::Engine.routes.append do

  # Admin routes
  namespace :partners, :path => '' do
    namespace :admin, :path => 'refinery' do
      resources :partners, :except => :show do
        collection do
          post :update_positions
        end
      end
    end
  end

end
