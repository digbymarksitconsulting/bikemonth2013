class CreatePartnersPartners < ActiveRecord::Migration

  def up
    create_table :refinery_partners do |t|
      t.string :name
      t.integer :logo_id
      t.string :url_path_regex
      t.integer :position

      t.timestamps
    end

  end

  def down
    if defined?(::Refinery::UserPlugin)
      ::Refinery::UserPlugin.destroy_all({:name => "refinerycms-partners"})
    end

    drop_table :refinery_partners

  end

end
