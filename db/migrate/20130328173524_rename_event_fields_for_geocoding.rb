class RenameEventFieldsForGeocoding < ActiveRecord::Migration
  def up
    rename_column :refinery_events, :lat, :latitude
    rename_column :refinery_events, :lon, :longitude
    rename_column :refinery_events, :location_raw, :address
  end

  def down
    rename_column :refinery_events, :latitude, :lat
    rename_column :refinery_events, :longitude, :lon
    rename_column :refinery_events, :address, :location_raw
  end
end
