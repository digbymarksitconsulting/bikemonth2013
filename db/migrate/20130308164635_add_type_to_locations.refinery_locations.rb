# This migration comes from refinery_locations (originally 4)
class AddTypeToLocations < ActiveRecord::Migration
  def change
    add_column :refinery_locations, :location_type, :string
  end
end