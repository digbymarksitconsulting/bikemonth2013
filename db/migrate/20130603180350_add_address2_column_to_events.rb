class AddAddress2ColumnToEvents < ActiveRecord::Migration
  def change
    add_column :refinery_events, :address2, :string
  end
end
