class AddEventTypeIdToEvents < ActiveRecord::Migration
  def change
    change_table :refinery_events do |t|
      t.references :refinery_events_event_type
    end
    remove_column :refinery_events, :event_type_id
    add_index :refinery_events, :refinery_events_event_type_id
  end
end