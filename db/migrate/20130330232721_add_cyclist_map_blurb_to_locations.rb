class AddCyclistMapBlurbToLocations < ActiveRecord::Migration
  def up
    add_column :refinery_locations, :cyclist_map_blurb, :text
  end
  def down
    remove_column :refinery_locations, :cyclist_map_blurb, :text
  end
end
