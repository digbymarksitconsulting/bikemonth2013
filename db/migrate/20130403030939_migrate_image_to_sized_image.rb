class MigrateImageToSizedImage < ActiveRecord::Migration
  def change

    puts "Currently there are #{Refinery::Image.count} images"

    Refinery::Image.all.each do |i|
      width = ((i.image.width * 55) / i.image.height).ceil
      height = 55
      i.logo_sized_image = i.image.thumb("#{width}x#{height}#")
      i.save
    end

    raise Exception('There are still images without a logo_sized_image') unless Refinery::Image.where(logo_sized_image_uid: nil).count==0

    puts "There are now #{Refinery::Image.count} images"

  end
end
