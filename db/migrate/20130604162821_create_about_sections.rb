class CreateAboutSections < ActiveRecord::Migration
  def up
    create_table :about_sections do |t|
      t.string :name
      t.timestamps
    end
    
    AboutSection.create :name => 'Lead Organizer'
    AboutSection.create :name => 'Lead Supporter'
    AboutSection.create :name => 'Local Supporter'
    AboutSection.create :name => 'Lead Partner'
    AboutSection.create :name => 'Official Prize Sponsor'
    AboutSection.create :name => 'Media Partner'
    AboutSection.create :name => 'Contributing Partner'
    AboutSection.create :name => 'Community Partner'
    
  end
  
  def down
    drop_table :about_sections
  end
end
