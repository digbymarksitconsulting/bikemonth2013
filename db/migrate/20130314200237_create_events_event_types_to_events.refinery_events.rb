# This migration comes from refinery_events (originally 10)
class CreateEventsEventTypesToEvents < ActiveRecord::Migration

  def up
    create_table :refinery_events_event_types_to_refinery_events_events, :id => false  do |t|
      t.references :refinery_events_event_types
      t.references :refinery_events

      t.timestamps
    end

  end

  def down
    if defined?(::Refinery::UserPlugin)
      ::Refinery::UserPlugin.destroy_all({:name => "refinerycms-events"})
    end

    drop_table :refinery_events_event_types_to_refinery_events_events

  end

end
