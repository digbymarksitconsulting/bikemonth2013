class SetupSideNavAndFooterPage < ActiveRecord::Migration
  def up
    if Refinery::Page.by_title("Side Nav").empty?
      side_nav_placeholder = ::Refinery::Page.create(:title => "Side Nav",
                                              :link_url => "/",
                                              :show_in_menu => false,
                                              :deletable => false)
      side_nav_placeholder.parts.create({
                                     :title => "Body",
                                     :body => "<p>IGNORE THIS! IT DOES NOT DO ANYTHING AND IS REQUIRED TO FEED THE SIDE NAV</p>",
                                     :position => 0
                                 })
    end
    if Refinery::Page.by_title("Footer").empty?
      side_nav_placeholder = ::Refinery::Page.create(:title => "Footer",
                                                     :link_url => "/",
                                                     :show_in_menu => false,
                                                     :deletable => false)
      side_nav_placeholder.parts.create({
                                            :title => "Body",
                                            :body => "<p>IGNORE THIS! IT DOES NOT DO ANYTHING AND IS REQUIRED TO FEED THE FOOTER</p>",
                                            :position => 0
                                        })
    end

  end

  def down
    if Refinery::Page.by_title("Side Nav").present?
      ::Refinery::Page.by_title('Side Nav').first.destroy!
    end
    if Refinery::Page.by_title("Footer").present?
      ::Refinery::Page.by_title('Footer').first.destroy!
    end
  end
end
