# This migration comes from refinery_events (originally 3)
class AddUserIdToEvents < ActiveRecord::Migration
  def change
    add_column :refinery_events, :user_id, :integer
    add_index :refinery_events, :user_id
  end
end