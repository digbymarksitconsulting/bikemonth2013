class RemoveTimeStampsFromJoinTables < ActiveRecord::Migration
  def up
    remove_timestamps :refinery_events_event_features_to_refinery_events_events
  end

  def down
    add_timestamps :refinery_events_event_features_to_refinery_events_events
  end
end
