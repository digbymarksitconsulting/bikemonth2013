class AddRsvpEmailAddressToEvents < ActiveRecord::Migration
  def change
    add_column :refinery_events, :rsvp_email, :string
    remove_column :refinery_events, :rsvp_required
  end
end
