class AddAboutSectionColumnToPartners < ActiveRecord::Migration
  
  def up
    
    add_column :refinery_partners, :about_section_id, :integer
    add_index :refinery_partners, :about_section_id
    
    add_index :refinery_partners, :name
    add_index :about_sections, :name
    
    set_section 'Cycle Toronto', 'Lead Organizer'
    set_section 'Smart Commute', 'Lead Supporter'
    
    ['Region of Peel', 'Hamilton', 'Toronto', 'York Region'].each do |p|
      set_section p, 'Local Supporter'
    end
    
    set_section 'McLeish Orlando', 'Lead Partner'
    
    set_section 'VIA Rail Canada', 'Official Prize Sponsor'
    set_section 'Welcome Cyclists', 'Official Prize Sponsor'
    
    ['Bixi', 'St. Lawrence Market', 'Sweet Pete\'s', 'Cycle Solutions',
      'Bateman\'s Bicycle Company', 'Timbuk2', 'Z', 'Jet Fuel Coffee'
    ].each do |p|
      set_section p, 'Contributing Partner'
    end
    
    ['Evergreen BrickWorks', 'Share The Road', 'TCAT'].each do |p|
      set_section p, 'Community Partner'
    end
    
  end
  
  def down
    remove_column :refinery_partners, :about_section_id
    
    remove_index :refinery_partners, :name
    remove_index :about_sections, :name
    
  end
  
  def set_section (partner_name_, section_name_)
    
    partner = Refinery::Partners::Partner.find_by_name(partner_name_)
    section = AboutSection.find_by_name(section_name_)
    
    if partner.present?
      
      if section.present?
        partner.about_section_id = section.id
        partner.save
      
      else
        puts "No section found with name '#{section_name_}'"
      end
      
    else
      help = ''
      if section.present?
        help = ".  (desired section was '#{section_name_}')"
      end
      puts "No partner found with name '#{partner_name_}'#{help}"
    end
    
  end
  
end
