# This migration comes from refinery_events (originally 4)
class AddAuthFieldsToEvents < ActiveRecord::Migration
  def change
    add_column :refinery_events, :admin_authorized, :boolean
  end
end