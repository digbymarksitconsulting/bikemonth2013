# This migration comes from refinery_events (originally 7)
class CreateEventsEventTypes < ActiveRecord::Migration

  def up
    create_table :refinery_events_event_types do |t|
      t.string :name, :null => false
      t.boolean :display, :null => false
      t.integer :position

      t.timestamps
    end

  end

  def down
    if defined?(::Refinery::UserPlugin)
      ::Refinery::UserPlugin.destroy_all({:name => "refinerycms-events"})
    end

    drop_table :refinery_events_event_types

  end

end
