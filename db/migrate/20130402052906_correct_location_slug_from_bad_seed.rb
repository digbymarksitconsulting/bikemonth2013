class CorrectLocationSlugFromBadSeed < ActiveRecord::Migration
  def up

    location_map = {'peel-bramptoncaledon' => 'peel',
                    'peel-mississauga' => 'mississauga',
                    'toronto-etobicoke-york' => 'etobicoke-york',
                    'toronto-scarborough' => 'scarborough',
                    'toronto-north-york' => 'north-york',
                    'toronto-downtown-east-york' => 'downtown-east-york'
    }

    location_map.each do |old_location_name, new_location_name|
      if (Refinery::Page.where(slug: old_location_name).present?)
        loc = Refinery::Page.where(slug: old_location_name).first
        loc.update_attribute(:slug, new_location_name)
      end
    end
  end

  def down

    location_map = {'peel-bramptoncaledon' => 'peel',
                    'peel-mississauga' => 'mississauga',
                    'toronto-etobicoke-york' => 'etobicoke-york',
                    'toronto-scarborough' => 'scarborough',
                    'toronto-north-york' => 'north-york',
                    'toronto-downtown-east-york' => 'downtown-east-york'
    }

    location_map.each do |old_location_name, new_location_name|
      if (Refinery::Page.where(slug: new_location_name).present?)
        loc = Refinery::Page.where(slug: new_location_name).first
        loc.update_attribute(:slug, old_location_name)
      end
    end
  end
end
