class AddFullNameAndPhoneToUser < ActiveRecord::Migration
  def change
    add_column :refinery_users, :full_name, :string
    add_column :refinery_users, :phone, :string
  end
end
