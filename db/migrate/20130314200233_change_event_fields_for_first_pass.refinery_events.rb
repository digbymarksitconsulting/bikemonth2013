# This migration comes from refinery_events (originally 6)
class ChangeEventFieldsForFirstPass < ActiveRecord::Migration
  def change
    add_column :refinery_events, :event_type_id, :integer
    add_column :refinery_events, :cost, :float
    add_column :refinery_events, :contact_email, :string
    add_column :refinery_events, :contact_phone, :string
    add_column :refinery_events, :url_1, :string
    add_column :refinery_events, :url_2, :string
    add_column :refinery_events, :url_3, :string
    add_column :refinery_events, :location_raw, :string
    add_column :refinery_events, :major_intersection, :string
    add_column :refinery_events, :city, :string
    add_column :refinery_events, :lat, :float
    add_column :refinery_events, :lon, :float
    add_column :refinery_events, :rsvp_required, :boolean
    add_column :refinery_events, :photo_id, :integer

    add_index :refinery_events, :event_type_id
  end
end