class AddBlurbToLocations < ActiveRecord::Migration
  def up
    add_column :refinery_locations, :tips_and_resources, :text
  end
  def down
    remove_column :refinery_locations, :tips_and_resources, :text
  end
end
