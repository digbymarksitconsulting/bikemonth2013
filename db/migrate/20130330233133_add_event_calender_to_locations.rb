class AddEventCalenderToLocations < ActiveRecord::Migration
  def up
    add_column :refinery_locations, :event_calendar_blurb, :text
  end
  def down
    remove_column :refinery_locations, :event_calendar_blurb, :text
  end
end
