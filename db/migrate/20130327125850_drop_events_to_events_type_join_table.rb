class DropEventsToEventsTypeJoinTable < ActiveRecord::Migration
  def up
    drop_table :refinery_events_event_types_to_refinery_events_events
  end

  def down
    create_table "refinery_events_event_types_to_refinery_events_events", :id => false, :force => true do |t|
      t.integer  "refinery_events_event_types_id"
      t.integer  "refinery_events_id"
      t.datetime "created_at",                     :null => false
      t.datetime "updated_at",                     :null => false
    end
  end
end
