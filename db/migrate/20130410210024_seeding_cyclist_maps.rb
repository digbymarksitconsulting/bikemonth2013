class SeedingCyclistMaps < ActiveRecord::Migration
  def up
    if ::Refinery::Locations::Location.where(slug: 'toronto').count == 1
      toronto = ::Refinery::Locations::Location.where(slug: 'toronto').first
      toronto.cyclist_map_blurb = '<ul><li><a href="http://www.toronto.ca/cycling/map/pdf/downtown_map.pdf">Downtown Toronto</a></li><li><a href="http://www.toronto.ca/cycling/map/pdf/front.pdf">Central Toronto, Etobicoke, and North York</a></li><li><a href="http://www.toronto.ca/cycling/map/pdf/back.pdf">Scarborough</a></li></ul>'
      toronto.save
    end

    if ::Refinery::Locations::Location.where(slug: 'hamilton').count == 1
      hamilton = ::Refinery::Locations::Location.where(slug: 'hamilton').first
      hamilton.cyclist_map_blurb = '<ul><li><a href="http://map.hamilton.ca/Static/PDFs/Public%20Works/Photoshop%20-%20Bikeways%20Map%20-%20March_2011%20-%20URBAN.pdf">Urban</a></li><li><a href="http://www.map.hamilton.ca/Static/PDFs/Public%20Works/Photoshop%20-%20Bikeways%20Map%20-%20March_2011%20-%20RURAL.pdf">Rural</a></li></ul>'
      hamilton.save
    end

    if ::Refinery::Locations::Location.where(slug: 'halton').count == 1
      halton = ::Refinery::Locations::Location.where(slug: 'halton').first
      halton.cyclist_map_blurb = '<ul><li><a href="http://www.halton.ca/common/pages/UserFile.aspx?fileId=81836">Halton Cycling Map</a></li></ul>'
      halton.save
    end

    if ::Refinery::Locations::Location.where(slug: 'york').count == 1
      york = ::Refinery::Locations::Location.where(slug: 'york').first
      york.cyclist_map_blurb = '<ul><li><a href="http://maps.york.ca/yorkexplorer/pdf/2011_CyclingMap.pdf">York Cycling Map</a></li></ul>'
      york.save
    end

    if ::Refinery::Locations::Location.where(slug: 'mississauga').count == 1
      mississauga = ::Refinery::Locations::Location.where(slug: 'mississauga').first
      mississauga.cyclist_map_blurb = '<ul><li><a href="http://www.walkandrollpeel.ca/map/themap.asp">Greater Peel Region</a></li><li><a href="http://www.mississauga.ca/file/COM/trails_map.pdf">Mississauga</a></li></ul>'
      mississauga.save
    end

    if ::Refinery::Locations::Location.where(slug: 'peel').count == 1
      peel = ::Refinery::Locations::Location.where(slug: 'peel').first
      peel.cyclist_map_blurb = '<ul><li><a href="http://www.walkandrollpeel.ca/map/themap.asp">Greater Peel Region</a></li><li><a href="http://www.mississauga.ca/file/COM/trails_map.pdf">Mississauga</a></li></ul>'
      peel.save
    end

    if ::Refinery::Locations::Location.where(slug: 'durham').count == 1
      durham = ::Refinery::Locations::Location.where(slug: 'durham').first
      durham.cyclist_map_blurb = '<ul><li><a href="http://www.durhamtourism.ca/brochures/DurhamCycleTours.pdf">Durham Cycling Map</a></li></ul>'
      durham.save
    end

  end

  def down
    if ::Refinery::Locations::Location.where(slug: 'toronto').count == 1
      toronto = ::Refinery::Locations::Location.where(slug: 'toronto').first
      toronto.cyclist_map_blurb = nil
      toronto.save
    end

    if ::Refinery::Locations::Location.where(slug: 'hamilton').count == 1
      hamilton = ::Refinery::Locations::Location.where(slug: 'hamilton').first
      hamilton.cyclist_map_blurb = nil
      hamilton.save
    end

    if ::Refinery::Locations::Location.where(slug: 'halton').count == 1
      halton = ::Refinery::Locations::Location.where(slug: 'halton').first
      halton.cyclist_map_blurb = nil
      halton.save
    end

    if ::Refinery::Locations::Location.where(slug: 'york').count == 1
      york = ::Refinery::Locations::Location.where(slug: 'york').first
      york.cyclist_map_blurb = nil
      york.save
    end

    if ::Refinery::Locations::Location.where(slug: 'mississauga').count == 1
      mississauga = ::Refinery::Locations::Location.where(slug: 'mississauga').first
      mississauga.cyclist_map_blurb = nil
      mississauga.save
    end

    if ::Refinery::Locations::Location.where(slug: 'peel').count == 1
      peel = ::Refinery::Locations::Location.where(slug: 'peel').first
      peel.cyclist_map_blurb = nil
      peel.save
    end

    if ::Refinery::Locations::Location.where(slug: 'durham').count == 1
      durham = ::Refinery::Locations::Location.where(slug: 'durham').first
      durham.cyclist_map_blurb = nil
      durham.save
    end


  end
end
