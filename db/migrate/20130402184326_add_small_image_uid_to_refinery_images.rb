class AddSmallImageUidToRefineryImages < ActiveRecord::Migration
  def change
    add_column :refinery_images, :small_image_uid, :string
  end
end
