class AddInitialNameToLocation < ActiveRecord::Migration
  def up
    add_column :refinery_locations, :initial_name, :string

    Refinery::Locations::Location.all.each do |loc|
      loc.initial_name = loc.name
      raise "#{loc.errors.full_messages.to_s}" unless loc.save(:validate => false)
    end

    change_column :refinery_locations, :initial_name, :string, :null => false

  end

  def down
    remove_column :refinery_locations, :initial_name
  end
end
