class ChangeUsernameAndPasswordToNullable < ActiveRecord::Migration
  def up
    change_column :refinery_users, :username, :string, :null => true
    change_column :refinery_users, :encrypted_password, :string, :null => true
  end

  def down
    change_column :refinery_users, :username, :string, :null => false
    change_column :refinery_users, :encrypted_password, :string, :null => false
  end
end
