# This migration comes from refinery_events (originally 2)
class AddLocationIdToEvents < ActiveRecord::Migration
  def change
    add_column :refinery_events, :location_id, :integer
    add_index :refinery_events, :location_id
  end
end