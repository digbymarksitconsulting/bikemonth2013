class RenameSmallImageToLogoImage < ActiveRecord::Migration
  def up
    rename_column :refinery_images, :small_image_uid, :logo_sized_image_uid
  end

  def down
    rename_column :refinery_images, :logo_sized_image_uid, :small_image_uid
  end
end
