class AddUserIdToRefineryImages < ActiveRecord::Migration
  def change
    add_column :refinery_images, :user_id, :integer, :null => false
  end
end
