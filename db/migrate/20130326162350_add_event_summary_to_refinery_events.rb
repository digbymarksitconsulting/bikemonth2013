class AddEventSummaryToRefineryEvents < ActiveRecord::Migration
  def change
    add_column :refinery_events, :summary, :string
  end
end
