class AddSponsorLinkToPartner < ActiveRecord::Migration
  def change
    add_column :refinery_partners, :sponsor_link, :string
  end
end
