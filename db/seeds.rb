# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

['all', Rails.env].each do |seed|
  seed_file = "#{Rails.root}/db/seeds/#{seed}.rb"
  if File.exists?(seed_file)
    puts "*** Loading #{seed} seed data"
    require seed_file
  end
end

#if defined?(Refinery::User)
#  puts "Setting up users"
#
#  user_aaron =  Refinery::User.create username: "aaron", email: "aaron.romeo@gmail.com", password: "s6ay0ut!", password_confirmation: "s6ay0ut!"
#  user_aaron.add_role :refinery
#  user_aaron.add_role :superuser
#  user_dave =  Refinery::User.create username: "dave", email: "dave.s.harper@gmail.com", password: "s6ay0ut!", password_confirmation: "s6ay0ut!"
#  user_dave.add_role :refinery
#  user_dave.add_role :superuser
#  user_events =  Refinery::User.create username: "events", email: "aaron.romeo+events@gmail.com", password: "test", password_confirmation: "test"
#  user_events.add_role :refinery
#  user_events.plugins = Refinery::User.general_users_plugins
#  user_events.confirm!
#
#
#  #Refinery::User.all.each do |user|
#  #  if user.plugins.where(:name => 'refinerycms-locations').blank?
#  #    user.plugins.create(:name => 'refinerycms-locations',
#  #                        :position => (user.plugins.maximum(:position) || -1) +1)
#  #  end
#  #end
#
#  puts "Finished setting up users"
#end

#if (Refinery::Partners::Partner.count == 0)
#
#  puts "Setting up partners"
#
#  metrolinx_logo = Refinery::Image.create :image => File.new(File.expand_path('../metrolinx_logoMn_edit.gif', __FILE__))
#  toronto_logo = Refinery::Image.create :image => File.new(File.expand_path('../toronto_logo_city.gif', __FILE__))
#  hamilton_logo = Refinery::Image.create :image => File.new(File.expand_path('../hamilton-logo-city.jpg', __FILE__))
#
#  Refinery::Partners::Partner.create name: "Metrolinx", logo_id: metrolinx_logo.id, url_path_regex: ".%2A", position: 0
#  Refinery::Partners::Partner.create name: "Hamilton", logo_id: hamilton_logo.id, url_path_regex: "%5C%2Flocations%5C%2Fhamilton%24", position: 1
#  Refinery::Partners::Partner.create name: "Toronto", logo_id: toronto_logo.id, url_path_regex: "%5C%2Flocations%5C%2Ftoronto%24", position: 2
#
#  puts "Finished setting up partners"
#
#end
#

#puts "Setting up locations"
#
#all_locations = Refinery::Locations::Location.create :name=> 'All Locations', :position => 0, :location_type => 'Top',
#                                                     :has_cyclist_profile => false, :has_event_calendar => false
#
#if all_locations.valid?
#
#  toronto = Refinery::Locations::Location.create :name=> 'Toronto', :parent_location_id => all_locations.id,
#                                                 :position => 1, :location_type => 'Region',
#                                                 :has_cyclist_profile => true, :has_event_calendar => false
#  (Refinery::Locations::Admin::LocationsController.
#      set_location_page toronto).save if toronto.valid?
#
#  hamilton = Refinery::Locations::Location.create :name=> 'Hamilton', :parent_location_id => all_locations.id,
#                                                  :position => 2, :location_type => 'Region',
#                                                  :has_cyclist_profile => true, :has_event_calendar => true
#  (Refinery::Locations::Admin::LocationsController.
#      set_location_page hamilton).save if hamilton.valid?
#
#  halton = Refinery::Locations::Location.create :name=> 'Halton', :parent_location_id => all_locations.id,
#                                                :position => 3, :location_type => 'Region',
#                                                :has_cyclist_profile => false, :has_event_calendar => true
#  (Refinery::Locations::Admin::LocationsController.
#      set_location_page halton).save if halton.valid?
#
#  durham = Refinery::Locations::Location.create :name=> 'Durham', :parent_location_id => all_locations.id,
#                                                :position => 4, :location_type => 'Region',
#                                                :has_cyclist_profile => false, :has_event_calendar => true
#  (Refinery::Locations::Admin::LocationsController.
#      set_location_page durham).save if durham.valid?
#
#  york = Refinery::Locations::Location.create :name=> 'York', :parent_location_id => all_locations.id,
#                                              :position => 5, :location_type => 'Region',
#                                              :has_cyclist_profile => false, :has_event_calendar => true
#  (Refinery::Locations::Admin::LocationsController.
#      set_location_page york).save if york.valid?
#
#  peel = Refinery::Locations::Location.create :name=> 'Peel', :parent_location_id => all_locations.id,
#                                              :position => 6, :location_type => 'Region',
#                                              :has_cyclist_profile => false, :has_event_calendar => true
#  (Refinery::Locations::Admin::LocationsController.
#      set_location_page peel).save if peel.valid?
#
#  mississauga = Refinery::Locations::Location.create :name=> 'Mississauga', :parent_location_id => peel.id,
#                                                     :position => 7, :location_type => 'Municipality',
#                                                     :has_cyclist_profile => true, :has_event_calendar => true
#  (Refinery::Locations::Admin::LocationsController.
#      set_location_page mississauga).save if mississauga.valid?
#
#  etobicoke = Refinery::Locations::Location.create :name=> 'Etobicoke-York', :parent_location_id => toronto.id,
#                                                   :position => 1, :location_type => 'Municipality',
#                                                   :has_cyclist_profile => false, :has_event_calendar => true
#  (Refinery::Locations::Admin::LocationsController.
#      set_location_page etobicoke).save if etobicoke.valid?
#
#  scarborough = Refinery::Locations::Location.create :name=> 'Scarborough', :parent_location_id => toronto.id,
#                                                     :position => 1, :location_type => 'Municipality',
#                                                     :has_cyclist_profile => false, :has_event_calendar => true
#  (Refinery::Locations::Admin::LocationsController.
#      set_location_page scarborough).save if scarborough.valid?
#
#  north_york = Refinery::Locations::Location.create :name=> 'North York', :parent_location_id => toronto.id,
#                                                    :position => 1, :location_type => 'Municipality',
#                                                    :has_cyclist_profile => false, :has_event_calendar => true
#  (Refinery::Locations::Admin::LocationsController.
#      set_location_page north_york).save if north_york.valid?
#
#  toronto_east_york = Refinery::Locations::Location.create :name=> 'Toronto-East York', :parent_location_id => toronto.id,
#                                                           :position => 1, :location_type => 'Municipality',
#                                                           :has_cyclist_profile => false, :has_event_calendar => true
#  (Refinery::Locations::Admin::LocationsController.
#      set_location_page toronto_east_york).save if toronto_east_york.valid?
#
#  puts "Finished up locations"

  #puts "Uploading images"
  #
  #charlie = Refinery::Image.create :image => File.new(File.expand_path('../charliebrown.jpg', __FILE__))
  #scooby = Refinery::Image.create :image => File.new(File.expand_path('../scooby-dooby.png', __FILE__))
  #
  #puts "Finished uploading images"
  #
  #puts "Setting up cyclists"
  #
  #Refinery::Locations::Cyclist.create name: 'Charlie Brown', occupation: 'Student', url_for_place_of_work: '',
  #                                    age: 15, photo_id: charlie.id, location_id: toronto.id
  #Refinery::Locations::Cyclist.create name: 'Scooby Doo', occupation: 'Pet', url_for_place_of_work: '',
  #                                    age: 7, photo_id: scooby.id, location_id: hamilton.id
  #
  #puts "Finished setting up cyclists"
  #
#  puts "Setting up test events"
#
#  Refinery::Events::Event.create title: "Bikeboom boxing High Park!", blurb: "<p>Bring your boom box! It's going to be a party in Etobicoke!</p>",
#                                 position: 0, start_at: "2013-05-08 16:00:00", end_at: "2013-05-08 20:00:00",
#                                 location_id: etobicoke.id, user_id: user_aaron.id, admin_authorized: true
#
#  Refinery::Events::Event.create title: "Bikeboom boxing Trinity Bellwood!", blurb: "<p>Bring your boom box! It's going to be a party in York!</p>",
#                                 position: 0, start_at: "2013-05-08 16:00:00", end_at: "2013-05-08 20:00:00",
#                                 location_id: toronto_east_york.id, user_id: user_dave.id, admin_authorized: true
#
#  Refinery::Events::Event.create title: "Bikeboom boxing Hamilton!", blurb: "<p>Bring your boom box! It's going to be a party in the Hammer!</p>",
#                                 position: 0, start_at: "2013-05-08 16:00:00", end_at: "2013-05-08 20:00:00",
#                                 location_id: hamilton.id, user_id: user_events.id, admin_authorized: true
#
#  Refinery::Events::Event.create title: "Bikeboom boxing Mississauga!", blurb: "<p>Bring your boom box! You don't want to 'Miss' out!</p>",
#                                 position: 0, start_at: "2013-05-08 16:00:00", end_at: "2013-05-08 20:00:00",
#                                 location_id: mississauga.id, user_id: user_events.id, admin_authorized: false
#
#  Refinery::Events::Event.create title: "Duke Bikes Week in Etobicoke!", blurb: "<p>This is a week long event</p>",
#                                 position: 0, start_at: "2013-05-13 00:00:00", end_at: "2013-05-19 23:59:59",
#                                 location_id: etobicoke.id, user_id: user_aaron.id, admin_authorized: true, photo: charlie
#
#  Refinery::Events::Event.create title: "The Hammer rings in Bike Month!", blurb: "<p>This is a full day event</p>",
#                                 position: 0, start_at: "2013-05-01 00:00:00", end_at: "2013-05-01 23:59:59",
#                                 location_id: hamilton.id, user_id: user_aaron.id, admin_authorized: true
#
#
#  Refinery::Events::Event.create title: "The Mississauga rings out Bike Month!", blurb: "<p>This is a full day event</p>",
#                                 position: 0, start_at: "2013-05-31 00:00:00", end_at: "2013-05-31 23:59:59",
#                                 location_id: hamilton.id, user_id: user_aaron.id, admin_authorized: true
#
#
#  puts "Finished setting up test events"
#
#else
#  puts "Top level location 'All Locations' did not get setup. This might be because this information already exists in the DB"
#end
