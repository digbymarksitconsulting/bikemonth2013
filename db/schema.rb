# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130604163945) do

  create_table "about_sections", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "about_sections", ["name"], :name => "index_about_sections_on_name"

  create_table "refinery_events", :force => true do |t|
    t.string   "title"
    t.text     "blurb"
    t.integer  "position"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.integer  "location_id"
    t.integer  "user_id"
    t.boolean  "admin_authorized"
    t.datetime "start_at"
    t.datetime "end_at"
    t.float    "cost"
    t.string   "contact_email"
    t.string   "contact_phone"
    t.string   "url_1"
    t.string   "url_2"
    t.string   "url_3"
    t.string   "address"
    t.string   "major_intersection"
    t.string   "city"
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "photo_id"
    t.integer  "refinery_events_event_type_id"
    t.string   "summary"
    t.string   "rsvp_email"
    t.string   "address2"
  end

  add_index "refinery_events", ["end_at"], :name => "index_refinery_events_on_end_at"
  add_index "refinery_events", ["location_id"], :name => "index_refinery_events_on_location_id"
  add_index "refinery_events", ["refinery_events_event_type_id"], :name => "index_refinery_events_on_refinery_events_event_type_id"
  add_index "refinery_events", ["start_at"], :name => "index_refinery_events_on_start_at"
  add_index "refinery_events", ["user_id"], :name => "index_refinery_events_on_user_id"

  create_table "refinery_events_event_features", :force => true do |t|
    t.string   "name",       :null => false
    t.boolean  "display",    :null => false
    t.integer  "position"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "refinery_events_event_features_to_refinery_events_events", :id => false, :force => true do |t|
    t.integer "refinery_events_event_features_id"
    t.integer "refinery_events_id"
  end

  create_table "refinery_events_event_types", :force => true do |t|
    t.string   "name",       :null => false
    t.boolean  "display",    :null => false
    t.integer  "position"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "refinery_images", :force => true do |t|
    t.string   "image_mime_type"
    t.string   "image_name"
    t.integer  "image_size"
    t.integer  "image_width"
    t.integer  "image_height"
    t.string   "image_uid"
    t.string   "image_ext"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.integer  "user_id",              :null => false
    t.string   "logo_sized_image_uid"
  end

  create_table "refinery_locations", :force => true do |t|
    t.string   "name"
    t.integer  "parent_location_id"
    t.integer  "position"
    t.integer  "page_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.string   "slug"
    t.string   "location_type"
    t.boolean  "has_cyclist_profile"
    t.boolean  "has_event_calendar"
    t.text     "tips_and_resources"
    t.string   "initial_name",         :null => false
    t.text     "cyclist_map_blurb"
    t.text     "event_calendar_blurb"
  end

  add_index "refinery_locations", ["slug"], :name => "index_refinery_locations_on_slug"

  create_table "refinery_locations_cyclists", :force => true do |t|
    t.string   "name"
    t.string   "occupation"
    t.string   "url_for_place_of_work"
    t.integer  "age"
    t.integer  "photo_id"
    t.integer  "location_id"
    t.text     "blurb"
    t.integer  "position"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  create_table "refinery_page_part_translations", :force => true do |t|
    t.integer  "refinery_page_part_id"
    t.string   "locale"
    t.text     "body"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
  end

  add_index "refinery_page_part_translations", ["locale"], :name => "index_refinery_page_part_translations_on_locale"
  add_index "refinery_page_part_translations", ["refinery_page_part_id"], :name => "index_refinery_page_part_translations_on_refinery_page_part_id"

  create_table "refinery_page_parts", :force => true do |t|
    t.integer  "refinery_page_id"
    t.string   "title"
    t.text     "body"
    t.integer  "position"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "refinery_page_parts", ["id"], :name => "index_refinery_page_parts_on_id"
  add_index "refinery_page_parts", ["refinery_page_id"], :name => "index_refinery_page_parts_on_refinery_page_id"

  create_table "refinery_page_translations", :force => true do |t|
    t.integer  "refinery_page_id"
    t.string   "locale"
    t.string   "title"
    t.string   "custom_slug"
    t.string   "menu_title"
    t.string   "slug"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "refinery_page_translations", ["locale"], :name => "index_refinery_page_translations_on_locale"
  add_index "refinery_page_translations", ["refinery_page_id"], :name => "index_refinery_page_translations_on_refinery_page_id"

  create_table "refinery_pages", :force => true do |t|
    t.integer  "parent_id"
    t.string   "path"
    t.string   "slug"
    t.boolean  "show_in_menu",        :default => true
    t.string   "link_url"
    t.string   "menu_match"
    t.boolean  "deletable",           :default => true
    t.boolean  "draft",               :default => false
    t.boolean  "skip_to_first_child", :default => false
    t.integer  "lft"
    t.integer  "rgt"
    t.integer  "depth"
    t.string   "view_template"
    t.string   "layout_template"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "refinery_pages", ["depth"], :name => "index_refinery_pages_on_depth"
  add_index "refinery_pages", ["id"], :name => "index_refinery_pages_on_id"
  add_index "refinery_pages", ["lft"], :name => "index_refinery_pages_on_lft"
  add_index "refinery_pages", ["parent_id"], :name => "index_refinery_pages_on_parent_id"
  add_index "refinery_pages", ["rgt"], :name => "index_refinery_pages_on_rgt"

  create_table "refinery_partners", :force => true do |t|
    t.string   "name"
    t.integer  "logo_id"
    t.string   "url_path_regex"
    t.integer  "position"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.string   "sponsor_link"
    t.integer  "about_section_id"
  end

  add_index "refinery_partners", ["about_section_id"], :name => "index_refinery_partners_on_about_section_id"
  add_index "refinery_partners", ["name"], :name => "index_refinery_partners_on_name"

  create_table "refinery_resources", :force => true do |t|
    t.string   "file_mime_type"
    t.string   "file_name"
    t.integer  "file_size"
    t.string   "file_uid"
    t.string   "file_ext"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "refinery_roles", :force => true do |t|
    t.string "title"
  end

  create_table "refinery_roles_users", :id => false, :force => true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "refinery_roles_users", ["role_id", "user_id"], :name => "index_refinery_roles_users_on_role_id_and_user_id"
  add_index "refinery_roles_users", ["user_id", "role_id"], :name => "index_refinery_roles_users_on_user_id_and_role_id"

  create_table "refinery_sponsors", :force => true do |t|
    t.string   "name"
    t.integer  "logo_id"
    t.integer  "position"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "refinery_sponsors_locations", :force => true do |t|
    t.integer  "sponsor_id"
    t.integer  "location_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "refinery_user_plugins", :force => true do |t|
    t.integer "user_id"
    t.string  "name"
    t.integer "position"
  end

  add_index "refinery_user_plugins", ["name"], :name => "index_refinery_user_plugins_on_name"
  add_index "refinery_user_plugins", ["user_id", "name"], :name => "index_refinery_user_plugins_on_user_id_and_name", :unique => true

  create_table "refinery_users", :force => true do |t|
    t.string   "username"
    t.string   "email",                  :null => false
    t.string   "encrypted_password"
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "sign_in_count"
    t.datetime "remember_created_at"
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "full_name"
    t.string   "phone"
  end

  add_index "refinery_users", ["confirmation_token"], :name => "index_refinery_users_on_confirmation_token", :unique => true
  add_index "refinery_users", ["id"], :name => "index_refinery_users_on_id"

  create_table "seo_meta", :force => true do |t|
    t.integer  "seo_meta_id"
    t.string   "seo_meta_type"
    t.string   "browser_title"
    t.string   "meta_keywords"
    t.text     "meta_description"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "seo_meta", ["id"], :name => "index_seo_meta_on_id"
  add_index "seo_meta", ["seo_meta_id", "seo_meta_type"], :name => "index_seo_meta_on_seo_meta_id_and_seo_meta_type"

end
