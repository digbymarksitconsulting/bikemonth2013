what = ENV['what'] || ''

if what.downcase.include?('partner_about_sections')
  puts 'Seeding partner about sections'
end

if what.downcase.include?('refinery')
  
  # Added by Refinery CMS Pages extension
  Refinery::Pages::Engine.load_seed

  # Added by Refinery CMS Events extension
  Refinery::Events::Engine.load_seed

  # Added by Refinery CMS Locations extension
  Refinery::Locations::Engine.load_seed

  # Added by Refinery CMS Partners extension
  Refinery::Partners::Engine.load_seed

  if defined?(Refinery::User)
    puts "Setting up users" unless Rails.env = 'test'

    user_aaron =  Refinery::User.create username: "aaron", email: "aaron.romeo@gmail.com", password: "s6ay0ut!", password_confirmation: "s6ay0ut!",
                                        full_name: "Aaron Romeo", phone: "647-123-1234"
    user_aaron.add_role :refinery
    user_aaron.add_role :superuser
    user_dave =  Refinery::User.create username: "dave", email: "dave.s.harper@gmail.com", password: "s6ay0ut!", password_confirmation: "s6ay0ut!",
                                       full_name: "Dave Harper", phone: "416-123-1234"
    user_dave.add_role :refinery
    user_dave.add_role :superuser

    puts "Finished setting up users" unless Rails.env = 'test'
  end

  puts "Setting up locations" unless Rails.env = 'test'

  all_locations = Refinery::Locations::Location.create :name=> 'All Locations', :position => 0, :location_type => 'Top',
                                                       :has_cyclist_profile => false, :has_event_calendar => false

  if all_locations.valid?

    toronto = Refinery::Locations::Location.create :name=> 'Toronto', :parent_location_id => all_locations.id,
                                                   :position => 1, :location_type => 'Region',
                                                   :has_cyclist_profile => true, :has_event_calendar => false
    (Refinery::Locations::Admin::LocationsController.
        set_location_page toronto).save if toronto.valid?

    hamilton = Refinery::Locations::Location.create :name=> 'Hamilton', :parent_location_id => all_locations.id,
                                                    :position => 2, :location_type => 'Region',
                                                    :has_cyclist_profile => true, :has_event_calendar => true
    (Refinery::Locations::Admin::LocationsController.
        set_location_page hamilton).save if hamilton.valid?

    halton = Refinery::Locations::Location.create :name=> 'Halton', :parent_location_id => all_locations.id,
                                                  :position => 3, :location_type => 'Region',
                                                  :has_cyclist_profile => false, :has_event_calendar => true
    (Refinery::Locations::Admin::LocationsController.
        set_location_page halton).save if halton.valid?

    durham = Refinery::Locations::Location.create :name=> 'Durham', :parent_location_id => all_locations.id,
                                                  :position => 4, :location_type => 'Region',
                                                  :has_cyclist_profile => false, :has_event_calendar => true
    (Refinery::Locations::Admin::LocationsController.
        set_location_page durham).save if durham.valid?

    york = Refinery::Locations::Location.create :name=> 'York', :parent_location_id => all_locations.id,
                                                :position => 5, :location_type => 'Region',
                                                :has_cyclist_profile => false, :has_event_calendar => true
    (Refinery::Locations::Admin::LocationsController.
        set_location_page york).save if york.valid?

    peel = Refinery::Locations::Location.create :name=> 'Peel', :parent_location_id => all_locations.id,
                                                :position => 6, :location_type => 'Region',
                                                :has_cyclist_profile => false, :has_event_calendar => true
    (Refinery::Locations::Admin::LocationsController.
        set_location_page peel).save if peel.valid?
    peel.name = 'Peel - Brampton/Caledon'
    peel.save

    mississauga = Refinery::Locations::Location.create :name=> 'Mississauga', :parent_location_id => peel.id,
                                                       :position => 7, :location_type => 'Municipality',
                                                       :has_cyclist_profile => true, :has_event_calendar => true
    (Refinery::Locations::Admin::LocationsController.
        set_location_page mississauga).save if mississauga.valid?
    mississauga.name = 'Peel - Mississauga'
    mississauga.save

    etobicoke = Refinery::Locations::Location.create :name=> 'Etobicoke-York', :parent_location_id => toronto.id,
                                                     :position => 1, :location_type => 'Municipality',
                                                     :has_cyclist_profile => false, :has_event_calendar => true
    (Refinery::Locations::Admin::LocationsController.
        set_location_page etobicoke).save if etobicoke.valid?
    etobicoke.name = 'Toronto - Etobicoke-York'
    etobicoke.save

    scarborough = Refinery::Locations::Location.create :name=> 'Scarborough', :parent_location_id => toronto.id,
                                                       :position => 1, :location_type => 'Municipality',
                                                       :has_cyclist_profile => false, :has_event_calendar => true
    (Refinery::Locations::Admin::LocationsController.
        set_location_page scarborough).save if scarborough.valid?
    scarborough.name = 'Toronto - Scarborough'
    scarborough.save

    north_york = Refinery::Locations::Location.create :name=> 'North York', :parent_location_id => toronto.id,
                                                      :position => 1, :location_type => 'Municipality',
                                                      :has_cyclist_profile => false, :has_event_calendar => true
    (Refinery::Locations::Admin::LocationsController.
        set_location_page north_york).save if north_york.valid?
    north_york.name = 'Toronto - North York'
    north_york.save

    toronto_east_york = Refinery::Locations::Location.create :name=> 'Toronto-East York', :parent_location_id => toronto.id,
                                                             :position => 1, :location_type => 'Municipality',
                                                             :has_cyclist_profile => false, :has_event_calendar => true
    (Refinery::Locations::Admin::LocationsController.
        set_location_page toronto_east_york).save if toronto_east_york.valid?
    toronto_east_york.name = 'Toronto - Downtown-East York'
    toronto_east_york.save

    puts "Finished up locations" unless Rails.env = 'test'

  else
    puts "There was a problem setting up the locations #{all_locations.errors.full_messages.to_s}"
  end

  if Refinery::Events::EventType.count == 0

    Refinery::Events::EventType.create :name => "Art / Film", :display => true
    Refinery::Events::EventType.create :name => "Clinic / Workshop", :display => true
    Refinery::Events::EventType.create :name => "Festival / Special Event", :display => true
    Refinery::Events::EventType.create :name => "Party / Social", :display => true
    Refinery::Events::EventType.create :name => "Presentation / Lecture", :display => true
    Refinery::Events::EventType.create :name => "Race", :display => true
    Refinery::Events::EventType.create :name => "Ride", :display => true

  end

  if Refinery::Events::EventFeature.count == 0

    Refinery::Events::EventFeature.create :name => "Accessible", :display => true
    Refinery::Events::EventFeature.create :name => "Partially accessible", :display => true
    Refinery::Events::EventFeature.create :name => "Parking", :display => true
    Refinery::Events::EventFeature.create :name => "Public washrooms", :display => true
    Refinery::Events::EventFeature.create :name => "Bike Parking", :display => true
    Refinery::Events::EventFeature.create :name => "On-site food or beverages", :display => true
    Refinery::Events::EventFeature.create :name => "New this year", :display => true

  end

  if Refinery::Partners::Partner.count == 0

    puts 'Setting up partners' unless Rails.env = 'test'

    p_all_locations = '.%2A'
    p_toronto_only = '%5C%2Flocations%5C%2Ftoronto%24'
    p_hamilton_only = '%5C%2Flocations%5C%2Fhamilton%24'

    # cycle toronto
    cycle_toronto_logo = Refinery::Image.create(
      :image => File.new("#{Rails.root}/db/seed_images/cycle-toronto.jpg"),
      :user_id => user_dave.id
    )
    Refinery::Partners::Partner.create(
      name: 'Cycle Toronto',
      logo_id: cycle_toronto_logo.id,
      url_path_regex: p_toronto_only
    )

    # mcleish
    mcleish_logo = Refinery::Image.create(
        :image => File.new("#{Rails.root}/db/seed_images/mcleish-orlando.jpg"),
        :user_id => user_dave.id
    )
    Refinery::Partners::Partner.create(
        name: 'McLeish Orlando',
        logo_id: mcleish_logo.id,
        url_path_regex: p_all_locations
    )

    # toronto
    toronto_logo = Refinery::Image.create(
        :image => File.new("#{Rails.root}/db/seed_images/toronto.jpg"),
        :user_id => user_dave.id
    )
    Refinery::Partners::Partner.create(
        name: 'City of Toronto',
        logo_id: toronto_logo.id,
        url_path_regex: p_toronto_only
    )

    # hamilton
    hamilton_logo = Refinery::Image.create(
        :image => File.new("#{Rails.root}/db/seed_images/hamilton.jpg"),
        :user_id => user_dave.id
    )
    Refinery::Partners::Partner.create(
        name: 'City of Hamilton',
        logo_id: hamilton_logo.id,
        url_path_regex: p_hamilton_only
    )

    # smart commute
    smart_commute_logo = Refinery::Image.create(
        :image => File.new("#{Rails.root}/db/seed_images/smart-commute.png"),
        :user_id => user_dave.id
    )
    Refinery::Partners::Partner.create(
        name: 'Smart Commute',
        logo_id: smart_commute_logo.id,
        url_path_regex: p_all_locations
    )

    cycle_toronto_logo = Refinery::Image.create(
        :image => File.new("#{Rails.root}/db/seed_images/cycle-toronto.jpg"),
        :user_id => user_dave.id
    )
    Refinery::Partners::Partner.create(
        name: 'Cycle Toronto',
        logo_id: cycle_toronto_logo.id,
        url_path_regex: p_toronto_only
    )

    # mcleish
    mcleish_logo = Refinery::Image.create(
        :image => File.new("#{Rails.root}/db/seed_images/mcleish-orlando.jpg"),
        :user_id => user_dave.id
    )
    Refinery::Partners::Partner.create(
        name: 'McLeish Orlando',
        logo_id: mcleish_logo.id,
        url_path_regex: p_all_locations
    )

    # toronto
    toronto_logo = Refinery::Image.create(
        :image => File.new("#{Rails.root}/db/seed_images/toronto.jpg"),
        :user_id => user_dave.id
    )
    Refinery::Partners::Partner.create(
        name: 'City of Toronto',
        logo_id: toronto_logo.id,
        url_path_regex: p_toronto_only
    )

    # hamilton
    hamilton_logo = Refinery::Image.create(
        :image => File.new("#{Rails.root}/db/seed_images/hamilton.jpg"),
        :user_id => user_dave.id
    )
    Refinery::Partners::Partner.create(
        name: 'City of Hamilton',
        logo_id: hamilton_logo.id,
        url_path_regex: p_hamilton_only
    )

    # smart commute
    smart_commute_logo = Refinery::Image.create(
        :image => File.new("#{Rails.root}/db/seed_images/smart-commute.png"),
        :user_id => user_dave.id
    )
    Refinery::Partners::Partner.create(
        name: 'Smart Commute',
        logo_id: smart_commute_logo.id,
        url_path_regex: p_all_locations
    )

    n = Refinery::Partners::Partner.all.count

    puts "Finished setting up #{n} partners" unless Rails.env = 'test'

    if ::Refinery::Locations::Location.where(slug: 'toronto').first
      toronto = ::Refinery::Locations::Location.where(slug: 'toronto').first
      toronto.cyclist_map_blurb = '<ul><li><a href="http://www.toronto.ca/cycling/map/pdf/downtown_map.pdf">Downtown Toronto</a></li><li><a href="http://www.toronto.ca/cycling/map/pdf/front.pdf">Central Toronto, Etobicoke, and North York</a></li><li><a href="http://www.toronto.ca/cycling/map/pdf/back.pdf">Scarborough</a></li></ul>'
      toronto.save
    end

    if ::Refinery::Locations::Location.where(slug: 'hamilton').first
      hamilton = ::Refinery::Locations::Location.where(slug: 'hamilton').first
      hamilton.cyclist_map_blurb = '<ul><li><a href="http://map.hamilton.ca/Static/PDFs/Public%20Works/Photoshop%20-%20Bikeways%20Map%20-%20March_2011%20-%20URBAN.pdf">Urban</a></li><li><a href="http://www.map.hamilton.ca/Static/PDFs/Public%20Works/Photoshop%20-%20Bikeways%20Map%20-%20March_2011%20-%20RURAL.pdf">Rural</a></li></ul>'
      hamilton.save
    end

    if ::Refinery::Locations::Location.where(slug: 'halton').first
      halton = ::Refinery::Locations::Location.where(slug: 'halton').first
      halton.cyclist_map_blurb = '<ul><li><a href="http://www.halton.ca/common/pages/UserFile.aspx?fileId=81836">Halton Cycling Map</a></li></ul>'
      halton.save
    end

    if ::Refinery::Locations::Location.where(slug: 'york').first
      york = ::Refinery::Locations::Location.where(slug: 'york').first
      york.cyclist_map_blurb = '<ul><li><a href="http://maps.york.ca/yorkexplorer/pdf/2011_CyclingMap.pdf">York Cycling Map</a></li></ul>'
      york.save
    end

    if ::Refinery::Locations::Location.where(slug: 'mississauga').first
      mississauga = ::Refinery::Locations::Location.where(slug: 'mississauga').first
      mississauga.cyclist_map_blurb = '<ul><li><a href="http://www.walkandrollpeel.ca/map/themap.asp">Greater Peel Region</a></li><li><a href="http://www.mississauga.ca/file/COM/trails_map.pdf">Mississauga</a></li></ul>'
      mississauga.save
    end

    if ::Refinery::Locations::Location.where(slug: 'peel').first
      peel = ::Refinery::Locations::Location.where(slug: 'peel').first
      peel.cyclist_map_blurb = '<ul><li><a href="http://www.walkandrollpeel.ca/map/themap.asp">Greater Peel Region</a></li><li><a href="http://www.mississauga.ca/file/COM/trails_map.pdf">Mississauga</a></li></ul>'
      peel.save
    end

    if ::Refinery::Locations::Location.where(slug: 'durham').first
      durham = ::Refinery::Locations::Location.where(slug: 'durham').first
      durham.cyclist_map_blurb = '<ul><li><a href="http://www.durhamtourism.ca/brochures/DurhamCycleTours.pdf">Durham Cycling Map</a></li></ul>'
      durham.save
    end

  end
else
  puts 'omitting refinery seeds'
end