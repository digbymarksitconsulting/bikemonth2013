#require 'faker'
what = ENV['what'] || ''

if what.downcase.include?('refinery')
  if defined?(Refinery::User)
    puts 'Setting up users...'

    user_events =  Refinery::User.create username: "events", email: "aaron.romeo+events@gmail.com",
                                         password: "test", password_confirmation: "test",
                                         full_name: "Test Event User", phone: "647-123-1234"
    user_events.add_role :refinery
    user_events.plugins = Refinery::User.general_users_plugins
    user_events.confirm!

    puts 'Finished setting up users'
  end

  user_aaron = Refinery::User.where(username: 'aaron').first
  user_dave = Refinery::User.where(username: 'dave').first
  user_events = Refinery::User.where(username: 'events').first

  puts 'SKIPPING cyclists.'

  puts 'Setting up test events...'

  unless (user_aaron.nil? || user_dave.nil? || user_events.nil? || Refinery::Events::Event.count > 0)

    Refinery::Events::Event.create title: "Bikeboom boxing High Park!", blurb: "<p>Bring your boom box! It's going to be a party in Etobicoke!</p>",
                                   position: 0, start_at: "2013-05-27 16:00:00", end_at: "2013-05-27 20:00:00",
                                   summary: "A picnic in Toronto",
                                   location_id: Refinery::Locations::Location.where(:slug => 'etobicoke-york').first.id,
                                   user_id: user_aaron.id, admin_authorized: true, city: 'Toronto',
                                   contact_email: user_aaron.email, refinery_events_event_type_id: Refinery::Events::EventType.first.id

    Refinery::Events::Event.create title: "Bikeboom boxing Trinity Bellwood!", blurb: "<p>Bring your boom box! It's going to be a party in York!</p>",
                                   position: 0, start_at: "2013-05-27 16:00:00", end_at: "2013-05-27 20:00:00",
                                   summary: "A picnic in Toronto",
                                   location_id: Refinery::Locations::Location.where(:slug => 'toronto-east-york').first.id,
                                   user_id: user_dave.id, admin_authorized: true, city: 'Toronto',
                                   contact_email: user_dave.email, refinery_events_event_type_id: Refinery::Events::EventType.first.id

    Refinery::Events::Event.create title: "Bikeboom boxing Hamilton!", blurb: "<p>Bring your boom box! It's going to be a party in the Hammer!</p>",
                                   position: 0, start_at: "2013-05-27 16:00:00", end_at: "2013-05-27 20:00:00",
                                   summary: "A picnic in Hamilton",
                                   location_id: Refinery::Locations::Location.where(:slug => 'hamilton').first.id,
                                   user_id: user_events.id, admin_authorized: true, city: 'Hamilton',
                                   contact_phone: user_events.phone, refinery_events_event_type_id: Refinery::Events::EventType.first.id

    Refinery::Events::Event.create title: "Bikeboom boxing Mississauga!", blurb: "<p>Bring your boom box! You don't want to 'Miss' out!</p>",
                                   position: 0, start_at: "2013-05-27 16:00:00", end_at: "2013-05-27 20:00:00",
                                   summary: "A picnic in Mississauga",
                                   location_id: Refinery::Locations::Location.where(:slug => 'mississauga').first.id,
                                   user_id: user_events.id, admin_authorized: false, city: 'Mississauga',
                                   contact_email: user_events.email, refinery_events_event_type_id: Refinery::Events::EventType.first.id

    Refinery::Events::Event.create title: "The Hammer rings in Bike Month!", blurb: "<p>This is a full day event</p>",
                                   position: 0, start_at: "2013-05-27 00:00:00", end_at: "2013-05-27 23:59:59",
                                   summary: "Kicking it off in style in the Hammer",
                                   location_id: Refinery::Locations::Location.where(:slug => 'hamilton').first.id,
                                   user_id: user_aaron.id, admin_authorized: true, city: 'Hamilton',
                                   contact_email: user_aaron.email, refinery_events_event_type_id: Refinery::Events::EventType.last.id


    Refinery::Events::Event.create title: "The Mississauga rings out Bike Month!", blurb: "<p>This is a full day event</p>",
                                   position: 0, start_at: "2013-05-27 00:00:00", end_at: "2013-05-27 23:59:59",
                                   summary: "Saying goodbye to Bike Month, but you can always ride!",
                                   location_id: Refinery::Locations::Location.where(:slug => 'hamilton').first.id,
                                   user_id: user_aaron.id, admin_authorized: true, city: 'Hamilton',
                                   contact_email: user_aaron.email, refinery_events_event_type_id: Refinery::Events::EventType.last.id


    (1..31).each do |i|
      (0..rand(3)).each do |j|
        fake_title = Faker::Lorem.sentence rand(8..10)
        fake_summary = Faker::Lorem.sentence rand(12..16)
        fake_blurb = "#{Faker::Lorem.paragraphs(rand(3..6)).join '</p><p>'}"
        start_time_h = rand(24)
        start_time_m = rand(4) * 15
        start_at = DateTime.parse("2013-05-#{i} #{start_time_h}:#{start_time_m}:00")
        if rand(5) < 4
          max_time = 96
        else
          max_time = 672
        end
        end_at = start_at + (Rational(1,24) * Rational(1,4) * rand(max_time))

        locations = Refinery::Locations::Location.select(:id)
        location_id = locations[rand(locations.size)].id
        users = Refinery::User.where('confirmed_at is not null').select(:id)
        types = Refinery::Events::EventType.all

        Refinery::Events::Event.create title: "#{fake_title}", blurb: "<p>#{fake_blurb}</p>",
                                       start_at: start_at.to_s, end_at: end_at.to_s,
                                       location_id: location_id,
                                       summary: fake_summary,
                                       user_id: users[rand(users.size)].id, admin_authorized: (rand(2)==1),
                                       city: Refinery::Locations::Location.find(location_id).name,
                                       contact_email: Faker::Internet.safe_email, refinery_events_event_type_id: types[rand(types.size)].id

      end
    end

  end

  n = Refinery::Events::Event.all.count

  puts "Finished setting up #{n} test events."
else
  puts 'omitting default dev seeds'
end