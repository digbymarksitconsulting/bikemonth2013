namespace :db do
  desc "Live DB Migrations"

  namespace :migrations do
    task :trunc => "db:load_config" do
      peel = Refinery::Locations::Location.where(:slug => 'peel').first
      peel.has_event_calendar = false
      peel.save
      brampton = Refinery::Locations::Location.create :name=> 'Brampton', :parent_location_id => peel.id, :position => 10, :location_type => 'Municipality', :has_cyclist_profile => true, :has_event_calendar => true
      (Refinery::Locations::Admin::LocationsController.
          set_location_page brampton).save if brampton.valid?
      caledon = Refinery::Locations::Location.create :name=> 'Caledon', :parent_location_id => peel.id, :position => 11, :location_type => 'Municipality', :has_cyclist_profile => true, :has_event_calendar => true
      (Refinery::Locations::Admin::LocationsController.
          set_location_page caledon).save if caledon.valid?

      Refinery::Events::Event.where(:location_id => peel.id).each do |event|
        event.location_id = brampton.id
        event.save
      end
    end
  end
end
