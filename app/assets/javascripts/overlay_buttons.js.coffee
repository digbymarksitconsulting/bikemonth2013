jQuery ->
  buttons = $('.overlayButton')
  if buttons.length > 0
    OverlayButtons.init_with buttons

OverlayButtons =

  init_with : (buttons_) ->
    buttons_.mouseover(
      () ->
        $(this).children('.overlay').stop().animate({top:'-50px'}, 250)

    ).mouseout(
      () ->
        $(this).children('.overlay').stop().animate({top:'-100px'}, 250)
    )