# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

# on document ready
jQuery ->
	if $('#Map').length > 0
		Home.initMap()


# home script
Home =

	_mapCenter : () ->
		new google.maps.LatLng(43.715535,-79.372101)

	_mapTypeId : () ->
		google.maps.MapTypeId.SATELLITE

	_mapOptions : () ->
		return {
			center: Home._mapCenter()
			zoom: 9
			mapTypeId: Home._mapTypeId()
			mapTypeControl: false
			panControl: false
			zoomControl: false
			scaleControl: false
			streetViewControl: false
			draggable: false
			scrollwheel: false
			disableDoubleClickZoom: true
		}

	_latLng : (lat_, lng_) ->
		new google.maps.LatLng(lat_, lng_)

	map : null

	initMap : () ->
		mapOptions = Home._mapOptions()
		Home.map = new google.maps.Map(document.getElementById("Map"), mapOptions);
		Home.addPolygons()
		Home.attachPolygonListeners()

	polygons : {}

	addPolygons : () ->
		Home.addPolygon 'toronto', '#ff8500', [
				Home._latLng(43.58238, -79.531403)
				Home._latLng(43.606251,-79.484711)
				Home._latLng(43.627129,-79.481964)
				Home._latLng(43.639057,-79.454498)
				Home._latLng(43.629117,-79.402313)
				Home._latLng(43.671845,-79.278717)
				Home._latLng(43.752249,-79.181213)
				Home._latLng(43.796872,-79.192200)
				Home._latLng(43.768119,-79.341888)
				Home._latLng(43.761176,-79.399567)
				Home._latLng(43.734391,-79.442139)
				Home._latLng(43.706601,-79.553375)
				Home._latLng(43.674825,-79.579468)
			]

		Home.addPolygon 'peel', '#4799eb', [
				Home._latLng(43.58536476400977, -79.5355224609375)
				Home._latLng(43.758200767075934, -79.62615966796875)
				Home._latLng(43.75522505306928, -79.81292724609375)
				Home._latLng(43.66389797397276, -79.91455078125)
				Home._latLng(43.476840397778915, -79.64263916015625)
				Home._latLng(43.49676775343911, -79.595947265625)
				Home._latLng(43.537598280094535, -79.59457397460938)
			]

		Home.addPolygon 'halton', '#98cb00', [
				Home._latLng(43.316185746193476, -79.8101806640625)
				Home._latLng(43.38309377382831, -79.70993041992188)
				Home._latLng(43.47086090917325, -79.63714599609375)
				Home._latLng(43.58635949637694, -79.81155395507812)
				Home._latLng(43.54257572246922, -79.8651123046875)
				Home._latLng(43.52365925541725, -79.9310302734375)
				Home._latLng(43.488797600050006, -79.98870849609375)
			]

		Home.addPolygon 'hamilton', '#34a83b', [
				Home._latLng(43.316185746193476, -79.8101806640625)
				Home._latLng(43.42699324866588, -79.98184204101562)
				Home._latLng(43.38508989465155, -80.09445190429688)
				Home._latLng(43.15109780818096, -80.03402709960938)
				Home._latLng(43.13907396889933, -80.0079345703125)
				Home._latLng(43.15109780818096, -79.95025634765625)
				Home._latLng(43.12003138678775, -79.80331420898438)
				Home._latLng(43.24520272203356, -79.74151611328125)
			]

		Home.addPolygon 'york', '#ff0000', [
				Home._latLng(43.67283828539733, -79.57878112792969)
				Home._latLng(43.71106791821881, -79.55131530761719)
				Home._latLng(43.73042168160789, -79.44625854492188)
				Home._latLng(43.762664060620736, -79.39201354980469)
				Home._latLng(43.768614600741934, -79.33639526367188)
				Home._latLng(44.090544164574716, -79.42291259765625)
				Home._latLng(44.132942183139654, -79.32540893554688)
				Home._latLng(44.322865609179345, -79.35836791992188)
				Home._latLng(44.30419567985762, -79.47372436523438)
				Home._latLng(44.276671273775186, -79.50393676757812)
				Home._latLng(44.201897151875094, -79.47509765625)
				Home._latLng(44.18121912526982, -79.64675903320312)
				Home._latLng(44.15462243076731, -79.727783203125)
			]

		Home.addPolygon 'durham', '#abcdef', [
				Home._latLng(43.75621697418049, -79.178466796875)
				Home._latLng(44.12998516884592, -79.30618286132812)
				Home._latLng(44.201897151875094, -78.94500732421875)
				Home._latLng(44.07476039341059, -78.93402099609375)
				Home._latLng(44.134913443750726, -78.64974975585938)
				Home._latLng(43.89690282163987, -78.58657836914062)
				Home._latLng(43.89195472686545, -78.65936279296875)
				Home._latLng(43.865227910689136, -78.7115478515625)
				Home._latLng(43.872158236415416, -78.80905151367188)
				Home._latLng(43.84641296470239, -78.87359619140625)
				Home._latLng(43.851364841841296, -78.94775390625)
				Home._latLng(43.82759208225603, -78.9752197265625)
				Home._latLng(43.807774213873806, -79.06448364257812)
				Home._latLng(43.82164741238288, -79.10018920898438)
				Home._latLng(43.76315996157264, -79.14276123046875)
			]

	addPolygon : (name_, color_, paths_) ->
		p = new google.maps.Polygon
			strokeColor: color_
			strokeOpacity: 0.9
			strokeWeight: 4
			fillColor: color_
			fillOpacity: 0.9
			paths: paths_
		p.name = name_
		p.color = color_
		p.setMap(Home.map)
		Home.polygons[name_] = p

	attachPolygonListeners : () ->

		for name, polygon of Home.polygons

			# NOTE the do (polygon) hack.
			# this wraps the polygon variable in a closure so that the referenced polygon
			# remains unique for each listener.
			# without this closure the polygon on the last iteration is used for all events >_<

			do (polygon) ->
				google.maps.event.addListener polygon, 'mouseover', (event) =>
					Home.mouseOverPolygon polygon
				google.maps.event.addListener polygon, 'mouseout', (event) =>
					Home.mouseOutPolygon polygon
				google.maps.event.addListener polygon, 'click', (event) =>
					Home.clickPolygon polygon

	_mouseOverPolygon : null

	mouseOverPolygon : (polygon_) ->
		if Home._mouseOverPolygon != polygon_
			Home.mouseOutPolygon(Home._mouseOverPolygon)
		Home._mouseOverPolygon = polygon_
		$(".regionInfo[data-region-name='#{ polygon_.name }']").show() if polygon_?
		polygon_.setOptions({fillColor:'white',strokeColor:'#ccc'})

	mouseOutPolygon : (polygon_) ->
		if polygon_? && Home._mouseOverPolygon == polygon_
			# hide the splash div for this polygon
			Home._mouseOverPolygon.setOptions({fillColor:polygon_.color,strokeColor:polygon_.color})
			Home._mouseOverPolygon = null
			$(".regionInfo[data-region-name='#{ polygon_.name }']").hide()

	clickPolygon : (polygon_) ->
		window.location = polygon_.name if polygon_?