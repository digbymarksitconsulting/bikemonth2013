jQuery ->
  if $('#Events').length > 0
    Events.init()

window.Events =

  monthNames : [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ]

  getMonthName : (index_) ->
    index_ += 12 while index_ < 0
    return Events.monthNames[index_ % 12]

  init : () ->

    $('#EventCalendarBreadcrumb').addClass 'selected'

    Events.calendarTemplate = Handlebars.compile $('#CalendarTemplate').html()

    Events.regionName = $('#EventsRegionName').html()

    Events.initCalendarEvents()

    Events.initFilterEvents()

    Events.initDates() # set start and end and current month

    Events.checkHash()

    $('#RideWithUs').click(
      (event_) ->
        event_.preventDefault()
        Events.checkHash()
    )

  checkHash : () ->
    if window.location.hash == '#btwd'
      Events.setCurrentMonth(5)
      setTimeout('Events.scrollToDate(5,27)', 100)

  initCalendarEvents : () ->
    $('#Calendar .day').live(
      'click'
      () ->
        date = $(this).data('date')
        month = $(this).parent().parent().data('month') + 1
        Events.scrollToDate month, date
    )

  initFilterEvents : () ->
    $('.event_type.button').click(
      () ->

        # Apply CSS to hide non-matching events
        $('.event_type.button').each(
          () ->
            $(this).removeClass 'selected'
            $('#Events').removeClass($(this).data('event-type'))
        )
        $(this).addClass 'selected'
        $('#Events').addClass($(this).data('event-type'))

        event_types = $(this).html()
        $('#EventsHeader').html("#{event_types} Events in #{Events.regionName}")

        # Hide whole days for non matching events
        $('#Events .day').show().each(
          () ->
            visible_children = $(this).children('.event:visible').length
            $(this).css('display', if visible_children > 0 then 'block' else 'none')
        )

        # update the side calendar
        Events.highlightDatesWithEvents()
    )

  initDates : () ->
    
    # Set up the start and end of valid month range
    Events.startMonth = 5
    Events.endMonth = 6
    
    Events.curDate = new Date()
    m = Events.curDate.getMonth() + 1
    d = Events.curDate.getDate()
    
    # current month must be within valid month range
    Events.setCurrentMonth(m)
    Events.scrollToDate(m,d)

  prevMonth : () ->
    Events.setCurrentMonth(Events.curMonth - 1)

  nextMonth : () ->
    Events.setCurrentMonth(Events.curMonth + 1)

  setCurrentMonth : (month_) ->
    Events.curMonth = month_
    if Events.curMonth < Events.startMonth
      Events.curMonth = Events.startMonth
    else if Events.curMonth > Events.endMonth
      Events.curMonth = Events.endMonth

    $('#Calendar').html(Events.getCalendarHtml 2013, Events.curMonth-1)

    if (Events.curMonth == Events.startMonth)
      $('#Calendar .prev').hide()
    if (Events.curMonth == Events.endMonth)
      $('#Calendar .next').hide()

    Events.highlightDatesWithEvents()

  highlightDatesWithEvents : () ->
    $('#Calendar .day').each(
      () ->
        date = $(this).data('date')
        if date
          month = Events.curMonth
          selector = "#DateHeader_0#{ month }_#{ date }"
          if $(selector).length > 0 && $(selector).parent().is(':visible')
            $(this).addClass 'with_events'
          else
            $(this).removeClass 'with_events'
    )

  scrollToDate : (month_, date_) ->
    selector = "#DateHeader_0#{ month_ }_#{ date_ }"
    header = $(selector)
    if header
      offset = header.offset()
      if offset
        top = offset.top - 56 # account for fixed header
        $(window).scrollTop(top)

  get : (callback_) ->

    m = 3
    y = 2013
    l = $('#Events').data('location')

    do (callback_) ->
      $.get(
        App.baseUrl + 'events.json'
        {month:m, year:y, location:l}
        callback_
      )

  populate : (json_, status_) ->
    if status_ is App.success

      # iterate through all events
      # {key, value} === {date, event}
      for date,event in json_.local_events
        Events.addEventToCalendarOnDate(event,date)

  addEventToCalendar : (event_) ->

    # iterate through the event's dates and add each one.
    startDate = Events.getDayOfMonth(event_.start_utc)
    endDate = Events.getDayOfMonth(event_.end_utc)

    # add each event to the calendar...
    for d in [startDate..endDate] by 1
      Events.addEventToCalendarOnDate event_, d

  addEventToCalendarOnDate : (event_, dayOfMonth_) ->
    dayDiv = $("#Events .day[data-date='#{ dayOfMonth_ }']")
    if dayDiv.length > 0

      # add the event to the dom
      n = 1 + parseInt(dayDiv.attr('data-num-events'), 10)
      dayDiv.attr('data-num-events', n)
      dayDiv.css('background-color', '#def')

      # add to our array too.
      if ( ! Events.events[dayOfMonth_])
        Events.events[dayOfMonth_] = [event_]
      else # assume the object is an array
        Events.events[dayOfMonth_].push event_

  getDayOfMonth : (utcSeconds_) ->
    return (new Date(utcSeconds_ * 1000)).getDate()

  getMonthFromUtc : (utcSeconds_) ->
    return (new Date(utcSeconds_ * 1000)).getMonth()

  ###
  # month 0-based
  ###
  getCalendarHtml : (year_, month_) ->

    month = {
      month : month_
      previous : Events.getMonthName(month_-1)
      next : Events.getMonthName(month_+1)
      title : Events.getMonthName(month_)
      weeks : []
    }

    firstDay = (new Date(year_, month_, 1)).getDay()
    lastDate = Events.numberOfDays year_, month_+1
    curWeek = []

    for day in [-firstDay+1..lastDate] by 1
      
      if curWeek.length == 7
        month.weeks.push {days:curWeek}
        curWeek = []

      curWeek.push({
        date: if day > 0 then day else ''
      })

    if curWeek.length > 0
      month.weeks.push {days:curWeek}

    return Events.calendarTemplate(month)

  # month 1 - 12.  (js 0-based checks the next month :S)
  numberOfDays : (year_, month_) ->
    d = new Date(year_, month_, 0)
    d.getDate()
