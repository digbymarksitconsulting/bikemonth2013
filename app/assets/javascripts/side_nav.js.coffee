jQuery ->
	if $('#SideNav').length > 0
		SideNav.init()

SideNav =

	top : 137

	fixedElements : '#SideNav, #SideCalendar, #EventsHeader, #EventSideBar'

	init : () ->
		$(window).scroll(
			() ->
				SideNav.fixPosition()
		)

		breadcrumbId = $('#BreadcrumbId')
		if breadcrumbId.length > 0
			s = '#' + breadcrumbId.data('breadcrumb-id')
			$(s).addClass 'selected'

		sideNavColumn = $('#SideNav').parent()
		sideNavColumn.css('height', sideNavColumn.height() + 'px')


	fixPosition : () ->
		windowTop = $(window).scrollTop()
		if windowTop > SideNav.top
			$(SideNav.fixedElements).addClass 'fixed'

			$('#SideCalendar, #EventSideBar').css('top', ($('#SideNav').height() + 32) + 'px')

			$('#EventsHeaderSpacer').show()

		else
			$(SideNav.fixedElements).removeClass 'fixed'
			$('#EventsHeaderSpacer').hide()