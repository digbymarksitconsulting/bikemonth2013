jQuery ->
	if $('#Overlays').length > 0
		Overlays.init()

window.Overlays =

	init : () ->
		$('#Overlays').click(
			(event_) ->
				event_.stopPropagation()
				Overlays.hide()
		)
		$('#Overlays .overlay').click(
			(event_) ->
				event_.stopPropagation()
		)

	show : (selector_) ->
		Overlays.hide()
		$('#Overlays').show()
		overlay = $(selector_)
		if overlay.length > 0
			overlay.show()
			h = overlay.height() + 128
			$('body').css('height', h + 'px')
			$('#OverlayCloseButton').show()

	hide : () ->
		$('#Overlays').hide()
		$('#OverlayCloseButton').hide()
		$('#Overlays .overlay').hide()