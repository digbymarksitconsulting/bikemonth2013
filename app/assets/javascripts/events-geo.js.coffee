google_maps_update = (event) ->
	event.preventDefault()
	id = $('#event_id').val()
	address = $('#address').val()
	city = $('#event_city').val()
	url = $('#url').val()
	$.ajax
		type: 'get'
		url: url
		data: { event: { id: id, address: address, city: city } }


$(document).ready ->
	$('#address').change (event) ->
		google_maps_update(event)

$(document).ready ->
	$('#event_city').change (event) ->
		google_maps_update(event)