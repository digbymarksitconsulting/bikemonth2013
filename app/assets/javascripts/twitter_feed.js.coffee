jQuery =>
	if $('#TwitterFeed').length > 0
		TwitterFeed.init()

window.TwitterFeed =

	init : () ->
		TwitterFeed.template = Handlebars.compile $('#TweetTemplate').html()
		TwitterFeed.load TwitterFeed.populate
		TwitterFeed.tweetsDiv = $('#TwitterFeed .tweets')

	load : (callbackFn_) ->
		do (callbackFn_) ->
			$.get(
				'/twitter/get_tweets.json'
				{}
				callbackFn_
			)

	populate : (response_) ->
		html = ''
		for tweet in response_
			tweet.text = tweet.text.replace(
				/(http:\/\/t.co\/[a-zA-Z0-9_]+)/g,
				"<a target=\"_blank\" href=\"$1\">$1</a>"
			)
			html += TwitterFeed.template(tweet)
		TwitterFeed.tweetsDiv.html(html)

		TwitterFeed.numTweets = response_.length
		TwitterFeed.tweetIndex = 0

		if TwitterFeed.numTweets > 2
			setInterval('fScrollFeed()', 8000)

window.fScrollFeed = () ->
	++ TwitterFeed.tweetIndex
	mt = 4 - (151 * TwitterFeed.tweetIndex)
	TwitterFeed.tweetsDiv.animate(
		{'margin-top': mt + 'px'},
		400,
		'swing'
		() ->
			if TwitterFeed.tweetIndex >= TwitterFeed.numTweets
				TwitterFeed.tweetIndex = 0
				TwitterFeed.tweetsDiv.hide().css('margin-top', '4px').fadeIn()
	)