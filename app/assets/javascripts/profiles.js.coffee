jQuery ->
	if $('#Profiles.side').length > 0
		SideProfiles.init()

window.SideProfiles =

	curProfileIndex : 0

	init : () ->

		SideProfiles.profiles = $('#Profiles.side .profile')

		if SideProfiles.profiles.length > 0
			SideProfiles.setup()

		else
			$('#Profiles .coming_soon').show()

	setup : () ->

		SideProfiles.profiles.click(
			() ->
				pid = $(this).data('profile-id')
				if pid
					window.location = '/locations/cyclists/' + pid
		)

		for i in [0..SideProfiles.profiles.length-1] by 1
			profile = $(SideProfiles.profiles[i])
			if i == 0
				profile.css('left', '0')
			else
				profile.css('left', '220px')

		SideProfiles.profiles.show()

		if SideProfiles.profiles.length > 1
			setInterval('SideProfiles.showNextProfile()', 9000)

	showNextProfile : () ->

		i = SideProfiles.curProfileIndex
		j = (i + 1) % SideProfiles.profiles.length

		$(SideProfiles.profiles[i]).animate(
			{left:'-220px'}
			500
			'swing'
			() ->
				$(this).css('left', '220px')
		)

		$(SideProfiles.profiles[j]).animate(
			{left:'0'}
			500
		)

		++ SideProfiles.curProfileIndex
		if SideProfiles.curProfileIndex >= SideProfiles.profiles.length
			SideProfiles.curProfileIndex = 0
