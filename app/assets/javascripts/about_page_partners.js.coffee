partners_div = null
desired_partner_area = 6000
no_params = {}

jQuery ->
  partners_div = $('#about_page_partners')
  if partners_div.length > 0
    init_about_page_partners()
    
init_about_page_partners = () ->
  
  $.getJSON(
    '/about_page_partners'
    no_params
    (response_) ->
      handle_response response_
  )
  
handle_response = (response_) ->
  
  for header, partners of response_
    
    set_max_logo_size(partners)
    
    partners_div.append "<h4>#{header}</h4>"
    append_partners_html(partners)
    partners_div.append '<div class="clr"></div>'
      
  window.Footer.fixPosition()

###
# Max logo size stuff
###
cell_size = null

set_max_logo_size = (partners_) ->
  
  cell_size = {width: 0, height: 0}
  
  for i in [0...partners_.length]
    s = partner_adjusted_size(partners_[i])
    cell_size.width  = Math.max(s.width, cell_size.width)
    cell_size.height = Math.max(s.height, cell_size.height)
  
append_partners_html = (partners_) ->
  for i in [0...partners_.length]
    partners_div.append partner_html(partners_[i])

partner_html = (partner_) ->
    
  size = partner_adjusted_size partner_
  "<#{partner_tag partner_} class='partner' style='#{size_style cell_size}'>#{partner_image_html partner_, size}</a>"

partner_tag = (partner_) ->
  if partner_.link
    "a href='#{partner_.link}'"
  else
    'div'
  
partner_image_html = (partner_, size_) ->
  offset = {
    x : .5 * (cell_size.width - size_.width)
    y : .5 * (cell_size.height - size_.height)
  }
  "<img src='#{partner_.image_path}' alt='#{partner_.name}' style='#{size_style size_} #{offset_style offset}' />"
  
partner_adjusted_size = (partner_) ->
  w = partner_.width
  h = partner_.height
  
  area = w * h
  factor = Math.sqrt(desired_partner_area / area)
  return {
    width  : (w * factor).toFixed(2)
    height : (h * factor).toFixed(2) 
  }

size_style = (size_) ->
  "width:#{size_.width}px;height:#{size_.height}px;"
  
offset_style = (offset_) ->
  "left:#{offset_.x}px;top:#{offset_.y}px;"