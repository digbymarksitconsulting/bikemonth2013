jQuery ->
  if $('#ToggleLocationDisplay').length > 0
    ToggleLocationDisplay.init()

ToggleLocationDisplay =

  init : () ->
    $('#ToggleLocationDisplay').click(
      () ->
        ToggleLocationDisplay.toggle()
    )

  toggle : () ->
    if $('#ToggleLocationDisplay .show').is(':visible')
      ToggleLocationDisplay.setShowing true
    else
      ToggleLocationDisplay.setShowing false

  setShowing : (showing_) ->
    $('#ToggleLocationDisplay .show').css 'display', (if showing_ then 'none' else 'inline')
    $('#ToggleLocationDisplay .hide').css 'display', (if showing_ then 'inline' else 'none')
    $('#ToggleLocationDisplay').siblings('.locations').css 'display', (if showing_ then 'block' else 'none')

