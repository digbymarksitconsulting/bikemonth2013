jQuery ->
  # enable direct pledge link
  if window.location.hash == '#pledge'
    window.gPledge.showPledge()

window.gPledge =
  click : () ->

    curDate = new Date()

    pledgeStartDate = new Date(2013,4,1)

    if curDate.getTime() > pledgeStartDate.getTime()
      gPledge.showPledge()

    else
      Overlays.show '#PledgeComingSoon'

  showPledge : () ->

    # first add the iframe to the pledge div.
    # We defer this to prevent on-load requests to the iframe content.
    if $('html .ie').length == 0
      t = Handlebars.compile($('#PledgeContentTemplate').html())
      $('#PledgeOverlay').html(t({}))
      Overlays.show '#PledgeOverlay'
    else
      window.open('https://secure.gotransit.com/smartcommute/BikeToWorkEntry.aspx')

  toggleLanguage : () ->
    link = $('#PledgeToggleLanguageLink')
    lang = if link.html() == 'English' then 'Français' else 'English'
    suffix = if lang == 'English' then '_fr.aspx' else '.aspx'
    url = "https://secure.gotransit.com/smartcommute/BikeToWorkEntry#{suffix}"
    $('#PledgeIframeContent').attr('src', url)
    link.html(lang)