jQuery ->
	if $('#Partners').length > 0
		Partners.init()

window.Partners =

	firstVisibleIndex : 0

	init : () ->

		Partners.logos = $('.partner_logo')

		Partners.setInitialPositions()

		Partners.show(0)

		if (Partners.logos.length > 4)
			setInterval('Partners.showNext()', 8000)

	setInitialPositions : () ->

		banner_height = 65

		for p in Partners.logos
			partner = $(p)
			margin_top = 0.5 * (banner_height - partner.height())
			partner.css('top', margin_top + 'px')
			partner.css('left', '960px')

	showNext : () ->
		++ Partners.firstVisibleIndex
		if Partners.firstVisibleIndex >= Partners.logos.length
			Partners.firstVisibleIndex = 0
		Partners.show(500)

	show : (dt_) ->

		banner_width = 930
		min_margin = 30

		partners_width = 0

		firstIndex = Partners.firstVisibleIndex
		lastIndex = Partners.firstVisibleIndex + Math.min(3, Partners.logos.length - 1)

		# on the first pass, determine the total width of all logos
		# and the required margin to evenly space them
		for logoIndex in [firstIndex..lastIndex]
			i = logoIndex % Partners.logos.length
			partner = $(Partners.logos[i])
			partners_width += partner.width()

		total_space = banner_width - partners_width

		margin = total_space / (lastIndex - firstIndex + 2)

		# now that we know the margin required between each logo, position each
		x = margin
		for logoIndex in [firstIndex..lastIndex]
			i = logoIndex % Partners.logos.length
			partner = $(Partners.logos[i])
			partner.animate(
				{left: "#{x}px"}
				dt_
			)
			x += partner.width() + margin

		if Partners.logos.length > 4
			leftmostIndex = Partners.firstVisibleIndex - 1
			if leftmostIndex < 0
				leftmostIndex += Partners.logos.length
			leftmostLogo = $(Partners.logos[leftmostIndex])
			x = -leftmostLogo.width()
			leftmostLogo.animate(
				{left: "#{x}px"},
				dt_,
				'swing',
				() ->
					$(this).css('left', '960px')
			)