jQuery ->
  if $('#Menu').length > 0
    Menus.init()

Menus =
  init : () ->
    $('#Menu .item').click(
      () -> window.location = $(this).data('href')
    )
    $('#Menu .dropDown').click(
      (event_) ->
        submenu = $(this).children('.submenu')
        if ! submenu.is(':visible')
          $('#Menu .submenu').hide()
          submenu.show()
          event_.stopPropagation()
    )
    $('body').click(
      () ->
        $('#Menu .submenu').hide()
    )