jQuery ->
	if $('#PageFooter').length > 0
		Footer.fixPosition()

window.Footer =
	fixPosition : () ->
		bh = $('body').height()
		wh = $(window).height()
		if bh < wh
			ww = $(window).width()
			$('#PageFooter').addClass('fixed').width(ww)
		else
		  $('#PageFooter').removeClass('fixed').width(null)