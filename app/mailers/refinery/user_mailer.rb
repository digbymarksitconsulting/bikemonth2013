module Refinery
  class UserMailer < ActionMailer::Base
    default :from => BikeMonth2013Constants::DEFAULT_FROM_EMAIL

    def reset_notification(user, request)
      @user = user
      @url = refinery.edit_refinery_user_password_url({
        #:host => request.host_with_port,
        :reset_password_token => @user.reset_password_token
      })

      mail(:to => user.email,
           :subject => t('subject', :scope => 'refinery.user_mailer.reset_notification')
      )
    end

    def admin_notification(event, title, fields, is_new)
      email = BikeMonth2013Constants::ADMIN_USER_EMAIL
      @url = refinery.edit_events_admin_event_url(event)
      @fields = fields

      if is_new
        subject = "An event has been created - #{title}"
      else
        subject = "An event has been updated - #{title}"
      end

      mail(:to => email,
           :subject => subject
      )
    end

    protected

    def url_prefix(request)
      "#{request.protocol}#{request.host_with_port}"
    end
  end
end
