
class ApplicationController < ActionController::Base
  protect_from_forgery
  
  def about_page_partners
    
    result = {}
    
    AboutSection.all.each do |s|
      
      partners_in_section = Refinery::Partners::Partner.where(:about_section_id => s.id)
      
      if partners_in_section.any?
        
        key = s.name.pluralize(partners_in_section.count)
        
        result[key] = partner_hashes(partners_in_section)
        
      end
      
    end
    
    render :json => result
  end
  
  def partner_hashes (partners_)
    
    hashes = []
  
    partners_.each do |p|
      hashes << partner_hash(p)
    end
    
    hashes
    
  end
  
  def partner_hash (partner_)
    return {
      :name       => partner_.name,
      :link       => partner_.sponsor_link,
      
      :width      => partner_.logo.image_width,
      :height     => partner_.logo.image_height,
      :image_path => partner_.logo.url
    }
  end
end
