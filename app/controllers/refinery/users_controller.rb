module Refinery
  class UsersController < Devise::RegistrationsController

    # Protect these actions behind an admin login
    before_filter :redirect?, :only => [:new, :create]

    helper Refinery::Core::Engine.helpers
    layout 'layouts/refinery/admin'

    def new
      @user = User.new
    end

    def create
      @user = User.new(params[:user])

      if @user.create_first

        if @user.has_role?(:superuser)
          flash[:message] = "<h2>#{t('welcome', :scope => 'refinery.users.create', :who => @user.username).gsub(/\.$/, '')}.</h2>".html_safe
          sign_in(@user)
          redirect_back_or_default(refinery.admin_root_path)
        else
          flash[:message] = "<b>Thanks #{@user.full_name}! Please check your email and click the confirmation link to complete your login. See you soon</b>".html_safe
          redirect_to new_refinery_user_session_path
        end
      else
        render :new
      end
    end

    protected

    def redirect?
      if refinery_user?
        redirect_to refinery.admin_users_path
      #elsif refinery_users_exist?
      #  redirect_to refinery.new_refinery_user_session_path
      end
    end

    def refinery_users_exist?
      Refinery::Role[:refinery].users.any?
    end

  end
end
