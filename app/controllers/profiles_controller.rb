class ProfilesController < ApplicationController
  include LocationsHelper

  def profiles_page

    @location, @other_locations = get_current_and_other_locations(params[:location], true)

    @profiles = Refinery::Locations::Cyclist.all

    if @profiles.empty?
      redirect_to refinery.root_path
    end

  end
end
