class TwitterController < ApplicationController

  def get_tweets
    tweets = prepare search_results()
    respond_to do |format|
      format.json { render :json => tweets }
      format.xml { render :xml => tweets.to_xml }
    end
  end

  def prepare(a)
    tweets = []
    a.each do |t|
      tweets << {
        ###
        # :inspect => t.inspect,
        ###
        :created_at => t.created_at,
        :text => t.text,
        :user => {
          :name => t.user.name,
          :screen_name => t.user.screen_name,
          :location => t.user.location,
          :description => t.user.description,
          :url => t.user.url,
          :profile_image_url => t.user.profile_image_url
        }
      }
    end
    tweets
  end

  def search_results
    
    search_string = [
      '#bikemonth2013',
      '#everyridecounts',
      'from:bikemonth2013'
    ].join(' OR ')

    Twitter.search(search_string, :lang => 'en', :count => 10).results

  end
end
