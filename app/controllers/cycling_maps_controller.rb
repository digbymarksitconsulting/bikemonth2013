class CyclingMapsController < ApplicationController
  include GenericExternalHelperHelper
  include LocationsHelper

  def maps_page

    @location, @other_locations = get_current_and_other_locations(params[:location], true)

    @profiles = Refinery::Locations::Cyclist.all

    # get partners
    @location_param = params[:location]
    @partners = get_partners @location_param
  end
end
