class HomeController < ApplicationController
  include GenericExternalHelperHelper

	def index
    # get partners
    @location_param = params[:location]
    @partners = get_partners @location_param
  end
end
