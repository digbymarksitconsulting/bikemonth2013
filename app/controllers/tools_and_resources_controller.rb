class ToolsAndResourcesController < ApplicationController
  include GenericExternalHelperHelper
  include LocationsHelper

  def tools_resources_page

    @location, @other_locations = get_current_and_other_locations(params[:location], true)

    # get partners
    @location_param = params[:location]
    @partners = get_partners @location_param

  end
end
