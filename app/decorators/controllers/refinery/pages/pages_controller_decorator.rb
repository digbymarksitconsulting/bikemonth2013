Refinery::PagesController.class_eval do
  include LocationsHelper
  before_filter :set_location
  def set_location
    params[:location] ||= 'all-locations'

    @location_param = params[:location]
    @location, @other_locations = get_current_and_other_locations(@location_param)
  end
end