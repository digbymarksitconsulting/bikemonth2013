require 'devise'
require 'friendly_id'

module Refinery
  class User < Refinery::Core::BaseModel
    extend FriendlyId

    has_and_belongs_to_many :roles, :join_table => :refinery_roles_users

    has_many :refinery_events, :class_name => '::Refinery::Events::Event'

    has_many :plugins, :class_name => "UserPlugin", :order => "position ASC", :dependent => :destroy
    friendly_id :username

    # Include default devise modules. Others available are:
    # :token_authenticatable, :lockable and :timeoutable
    if self.respond_to?(:devise)
      devise :database_authenticatable, :registerable, :recoverable, :rememberable, :confirmable,
             :trackable, :validatable, :authentication_keys => [:login]
    end

    # Setup accessible (or protected) attributes for your model
    # :login is a virtual attribute for authenticating by either username or email
    # This is in addition to a real persisted field like 'username'
    attr_accessor :login
    attr_accessible :email, :password, :password_confirmation, :remember_me, :username, :plugins, :login,
                    :phone, :full_name, :confirm_user
    validates :phone, :presence => true, :allow_blank => false
    validates :full_name, :presence => true, :allow_blank => false
    validates :phone, :format => {:with => BikeMonth2013Constants::PHONE_VALIDATE_REGEX}


    validates :username, :presence => true, :uniqueness => true
    before_validation :downcase_username

    class << self
      # Find user by email or username.
      # https://github.com/plataformatec/devise/wiki/How-To:-Allow-users-to-sign_in-using-their-username-or-email-address
      def find_for_database_authentication(conditions)
        value = conditions[authentication_keys.first]
        where(["username = :value OR email = :value", { :value => value }]).first
      end

      def general_users_plugins
        ['refinery_dashboard', 'events', 'refinery_users', 'refinery_images']
      end

      def to_csv
        columns = {
            "user" => {
                "Username" => "username",
                "E-mail" => "email",
                "Full Name" => "full_name",
                "Phone" => "phone",
                "Sign In Count" => "sign_in_count",
                "Created At" => "created_at",
                "Updated At" => "updated_at",
                "Confirmed At" => "confirmed_at",
            },
            "event" => {
              "Event ID" => "id",
              "Title" => "title",
              "Blurb" => "blurb",
              "Created At" => "created_at",
              "Updated At" => "updated_at",
              "Admin Authorized" => "admin_authorized",
              "Event Start" => "start_at",
              "Event End" => "end_at",
              "Cost" => "cost",
              "Event Contact Email" => "contact_email",
              "Event Contact Phone" => "contact_phone",
              "URL 1" => "url_1",
              "URL 2" => "url_2",
              "URL 3" => "url_3",
              "Address 1" => "address",
              "Address 2" => "address2",
              "Major Intersection" => "major_intersection",
              "City" => "city",
              "Latitude" => "latitude",
              "Longitude" => "longitude",
              "Summary" => "summary",
              "RSVP Email" => "rsvp_email",
            },
            "location" => {
                "Location" => "name"
            },
            "eventtype" => {
                "Event Type" => "name"
            },
            "eventfeature" => {
                "Event Features" => "name"
            }
        }

        CSV.generate do |csv|

          header = columns['user'].keys + columns['event'].keys +
              columns['location'].keys + columns['eventtype'].keys +
              columns['eventfeature'].keys
          csv << header
          all.each do |user|
            events = user.refinery_events.includes(:event_features, :event_type, :location).all
            if events.size == 0
              csv << user.attributes.values_at(*columns['user'].values)
            else
              events.each do |event|
                line = user.attributes.values_at(*columns['user'].values)
                line += event.attributes.values_at(*columns['event'].values)
                line += event.location.attributes.values_at(*columns['location'].values)
                line += event.event_type.attributes.values_at(*columns['eventtype'].values)
                features = ''
                event.event_features.each do |feature|
                  features += ', ' if features.length > 0
                  features << feature.attributes.values_at(*columns['eventfeature'].values)[0]
                end
                line << features
                csv << line
              end
            end
          end
        end
      end

    end

    def confirm_user=(value)
      self.confirm! if value
    end

    def confirm_user
      confirmed?
    end

    def password_required?
      super if confirmed?
    end

    def password_match?
      self.errors[:password] << "can't be blank" if password.blank?
      self.errors[:password_confirmation] << "can't be blank" if password_confirmation.blank?
      self.errors[:password_confirmation] << "does not match password" if password != password_confirmation
      password == password_confirmation && !password.blank?
    end

    def plugins=(plugin_names)
      if persisted? # don't add plugins when the user_id is nil.
        UserPlugin.delete_all(:user_id => id)

        plugin_names.each_with_index do |plugin_name, index|
          plugins.create(:name => plugin_name, :position => index) if plugin_name.is_a?(String)
        end
      end
    end

    def authorized_plugins
      plugins.collect(&:name) | ::Refinery::Plugins.always_allowed.names
    end

    def can_delete?(user_to_delete = self)
      user_to_delete.persisted? &&
        !user_to_delete.has_role?(:superuser) &&
        ::Refinery::Role[:refinery].users.any? &&
        id != user_to_delete.id
    end

    def can_edit?(user_to_edit = self)
      user_to_edit.persisted? && (
        user_to_edit == self ||
        self.has_role?(:superuser)
      )
    end

    def add_role(title)
      raise ArgumentException, "Role should be the title of the role not a role object." if title.is_a?(::Refinery::Role)
      roles << ::Refinery::Role[title] unless has_role?(title)
      self.confirm! if has_role?(:superuser)
    end

    def has_role?(title)
      raise ArgumentException, "Role should be the title of the role not a role object." if title.is_a?(::Refinery::Role)
      roles.any?{|r| r.title == title.to_s.camelize}
    end

    def create_first
      if valid?
        # first we need to save user
        save
        # add refinery role
        add_role(:refinery)
        # add superuser role if there are no other users
        add_role(:superuser) if ::Refinery::Role[:refinery].users.count == 1
        # add plugins
        self.plugins = has_role?(:superuser) ? Refinery::Plugins.registered.in_menu.names :
            Refinery::User.general_users_plugins
      end

      # return true/false based on validations
      valid?
    end

    def to_s
      username.to_s
    end

    def to_param
      to_s.parameterize
    end

    private
    # To ensure uniqueness without case sensitivity we first downcase the username.
    # We do this here and not in SQL is that it will otherwise bypass indexes using LOWER:
    # SELECT 1 FROM "refinery_users" WHERE LOWER("refinery_users"."username") = LOWER('UsErNAME') LIMIT 1
    def downcase_username
      self.username = self.username.downcase if self.username?
    end

  end
end
