BikeMonth2013::Application.routes.draw do
  
  get "test/index"

  match '/about_page_partners' => 'application#about_page_partners'
  
  get 'cycling_maps/maps_page'
  match '/cycling_maps' => 'cycling_maps#maps_page'

  get 'profiles/profiles_page'
  match '/profiles' => 'profiles#profiles_page'

  get 'tips/index'
  match '/tips_and_resources' => 'tips#index'

  get 'tools_and_resources/tools_resources_page'
  match '/tools_and_resources' => 'tools_and_resources#tools_resources_page'

  get 'twitter/get_tweets'

  get 'city/index'
  match '/city' => 'city#index'

	get 'home/index' # ?
  match '/home' => 'home#index' # ?
  match '/' => 'home#index' # ?

  Refinery::Core::Engine.routes.draw do

  get "test/index"

  get 'cycling_maps/maps_page'

  get 'profiles/index'

  get 'tips/index'

  Refinery::Core::Engine.routes.append do
    namespace :admin, :path => 'refinery' do
      resources :users, :except => :show do
        collection do
          get :report
        end
      end
    end
  end

    begin
      require 'devise'
      devise_for :refinery_user,
                 :class_name => 'Refinery::User',
                 :path => 'refinery/users',
                 :controllers => { :registrations => 'refinery/users', :confirmations => 'refinery/confirmations'},
                 :skip => [:registrations, :confirmations],
                 :path_names => { :sign_out => 'logout',
                                  :sign_in => 'login',
                                  :sign_up => 'register',
                                  :confirmation => 'confirmation' }

      # Override Devise's other routes for convenience methods.
      devise_scope :refinery_user do
        get '/refinery/login', :to => "sessions#new", :as => :new_refinery_user_session
        get '/refinery/logout', :to => "sessions#destroy", :as => :destroy_refinery_user_session
        get '/refinery/users/register' => 'users#new', :as => :new_refinery_user_registration
        post '/refinery/users/register' => 'users#create', :as => :refinery_user_registration
        put '/refinery/users/confirmation' => 'confirmations#confirm', :as => :refinery_user_confirmation
        #get '/refinery/users/confirmation', :to => 'confirmations#show', :as => :get_refinery_user_confirmation
      end
    rescue RuntimeError => exc
      if exc.message =~ /ORM/
        # We don't want to complain on a fresh installation.
        if (ARGV || []).exclude?('--fresh-installation')
          puts "---\nYou can safely ignore the following warning if you're currently installing Refinery as Devise support files have not yet been copied to your application:\n\n"
          puts exc.message
          puts '---'
        end
      else
        raise exc
      end
    end
  end

  mount Refinery::Core::Engine, :at => '/'

  # This line mounts Refinery's routes at the root of your application.
  # This means, any requests to the root URL of your application will go to Refinery::PagesController#home.
  # If you would like to change where this extension is mounted, simply change the :at option to something different.
  #
  # We ask that you don't use the :as option here, as Refinery relies on it being the default of "refinery"

end
